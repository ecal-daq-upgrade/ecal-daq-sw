#ifndef RPCTEST
#define RPCTEST
#include <wiscrpcsvc.h>
#include <nlohmann/json.hpp>
#include <log4cplus/log4cplus.h>

using json = nlohmann::json;
using namespace wisc;


//int testRpcConnection(std::string hostname);
void openRpcConnection(RPCSvc& rpc, std::string hostname);
//json readReagister(log4cplus::Logger logger, std::string hostname, std::string towers);
int getallregisters(log4cplus::Logger logger, RPCSvc &rpc, std::string towers, std::string &jsonresponse);
int configureFesFromSingleFile(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers);
int resetVfes(log4cplus::Logger logger, RPCSvc &rpc, std::string towers);
int prepareDTUAlign(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers);
int configureVfesFromSingleFile(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers);

#endif
