#ifndef BITMAP_H
#define BITMAP_H

//#include <bitset>
#include <stdint.h>
#include <string>
#include <map>
#include <iostream> 
#include <boost/dynamic_bitset.hpp>

class bitmap {


 public:
  bitmap(); 
  bitmap(size_t sz); 

  void print_bits();

  std::string description;
  std::string name;
  uint8_t access;

  boost::dynamic_bitset<> pon_reset_value; 
  boost::dynamic_bitset<> bits; 
  

}; 



#endif 
