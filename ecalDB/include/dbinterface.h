#ifndef BCPdb_H
#define BCPdb_H


#include <iostream> 
#include <stdexcept>
#include <string> 
#include <sstream> 
#include <fstream>
#include <vector> 
#include <stdint.h>
#include <map> 
#include <memory>
#include <set> 
#include "vfe.h"


namespace bcp {


  typedef std::pair<int,int> keyid_pair;

  int calculate_channel_id(int channel, int sm_id, int tower_id); 

  enum dbType {
    Base,
    SQLite,
    Oracle,
    Postgresql
  }; 

  const std::map<dbType, std::string> dbNames =
    {
      {Base , "Base DB" },
      {SQLite , "SQLite backend" },
      {Oracle , "Oracle backend" },
      {Postgresql , "PostgreSQL backend" }
    }; 

  const std::vector<std::string> tableorder = { "supermodules", "towers", "board_types", "boards",
						"channels", "device_types", "devices",
						"register_types", "registers", "register_sets",
						"register_set_map", "bitmaps" }; 

  const std::vector<std::string> vfetableorder = { "adcs", "calibration_set_map", "pedestals", 
                        "pedestal_set_map", "catia_defaults", "dtu_defaults", "lpgbt_defaults", "vfe_config_map"}; 

  class bcpdb {
  public:

    bcpdb();
    virtual ~bcpdb();


    // We'll want to replace the string uri with some proper File / URI Parsing Library
    // We want to handle the case of files (for SQLite) and Network / Other for other DBs
    bcpdb(const std::string uri, bool create);
    virtual bool open(const std::string &uri, bool create);
    virtual bool close(); 


    virtual void create_table(const std::string &ctstmt);
    virtual bool create_tables();
    bool create_views();


    std::vector<std::string> tables(); 

    bool fill_tables(); 

    bool check_key(const std::string &key);

    virtual void addBoard(int bkey, int btype, int tkey, int bcode); 
    virtual void addBoardType(int id, std::string &name, std::string &desc);
    virtual void addBoardTypes(std::map<int, std::pair<std::string, std::string>> &board_types);
    virtual void addRegisterType(Register_Type &r);
    virtual int addRegister(Register &r);
    virtual void addRegisterFast(std::vector<Register> &vr);
    virtual int addRegisterSet();
    virtual void addSetMapEntry(int register_set_id, int register_id);
    virtual int addDevice(const Device &d);
    virtual int addCATIA(const Catia &d);
    virtual void addDeviceType(Device_Type &dt);
    virtual void addChannel(int ch_key, int ch_id, int board_key);
    
    virtual void addTower(unsigned int tower, unsigned int sm);
    virtual void addSuperModule(unsigned int id);
    virtual void save(const std::string &fname);

    virtual int getDeviceFromTypeAndKey(int ch_key, device device_type);
    virtual std::vector<int> getBoardChannels(int board_type, int tower_id, int supermodule); 
    virtual std::vector<int> getSupermodules();     
    virtual std::vector<keyid_pair> getTowers();
    virtual std::vector<Device> devices();
    virtual std::vector<Device> devices(int channel_id); 
    virtual std::vector<Device_Type> device_types();
    virtual std::vector<Register> registers();
    virtual std::vector<Register> registers(int device_id); 
    virtual std::vector<Register_Type> register_types();
    virtual std::vector<Register_Type> register_types(device type);

    virtual std::vector<keyid_pair > channels(); // return all channel keys and ids
    virtual std::vector<keyid_pair> channels(int board_type, int tower_id, int supermodule);


    dbType _type;
    
    //  private:

    
  };

  
}


#endif 
