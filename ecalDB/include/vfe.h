#ifndef VFE_H
#define VFE_H
#include <iostream>
#include <map> 
#include <string>
#include <stdint.h>
#include <nlohmann/json.hpp>

//#include "bitmap.h"

namespace bcp { 
  // In general trying to match the database tables to c++ classes 

  enum device {
    ADCH =      0,
    ADCL =      1,
    DTU =       2,
    CATIA =     3,
    LPGBT0 =    4,
    LPGBT1 =    5,
    LPGBT2 =    6,
    LPGBT3 =    7,
    VTRX =      8,
    SCA =       9,
    Unknown =   99
  }; 


  device lookup_device_type(int type);

  class Register_Type {
  public:
    Register_Type();
    ~Register_Type() {};

    std::string name;
    unsigned int register_type_id; 
    unsigned int address;
    unsigned int width;
    unsigned int initial; 
    device device_type;
    uint8_t access;
    
    std::string description; 
    //std::vector<bitmap> bitmasks; 

  };
  
  class Register {

  public:
    Register();
    ~Register() {};

    Register_Type type; 
    unsigned int rvalue;
    unsigned int device_id; 

  };



  class Device_Type {
  public:
    Device_Type();
    ~Device_Type() { };
  
    std::string name;
    std::string description; 
    device type; 

  }; 

  class Device {
  public: 
    Device();
    virtual ~Device() {}; 
    Register lookup(const std::string &rname);
    void print_registers();
    bool load_from_csv(const std::string csvfile);
    void insert(const Register &reg);

    unsigned int device_id; 
    unsigned int ch_id; 
    std::map<std::string, Register> registers; 
    Device_Type dtype; 

  };

  class Catia:  public Device {
  public:
    Catia() : Device() { channel = 0; };
    Catia(int channel) : Device() { channel = channel;}; 
    unsigned int channel; 


  };

}; 


#endif 
