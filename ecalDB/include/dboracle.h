#ifndef dbORACLE_H
#define dbORACLE_H

#include "occi.h"
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <sstream> 
#include <fstream>
#include <vector> 
#include <stdint.h>
#include <map> 
#include <memory>
#include <set> 

#include "vfe.h"
#include "dbinterface.h"
#include <nlohmann/json.hpp>
#include "log4cplus/logger.h"
using json = nlohmann::json;
using namespace oracle::occi;

namespace bcp { 

  namespace myoracle {

    typedef std::pair<int,int> keyid_pair; 

      const std::map<std::string, std::string> ctablestrs = {
    {"bitmaps",  "CREATE TABLE bitmaps ( id number generated as identity PRIMARY KEY, \n"
     "address INTEGER NOT NULL, \n"
     "register_type_id INTEGER NOT NULL, \n"
     "name VARCHAR(255) NOT NULL, \n"
     "description VARCHAR(255) NOT NULL, \n"
     "mask INTEGER NOT NULL, \n"
     "access_type INTEGER NOT NULL, CHECK(access_type >= 0 and access_type <= 3), \n"
     "FOREIGN KEY (register_type_id) REFERENCES register_types(register_type_id))"},

    {"register_sets", "CREATE TABLE register_sets ( register_set_id number generated as identity PRIMARY KEY, \n"
     "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
     "notes VARCHAR(255))"},

    {"register_set_map", "CREATE TABLE register_set_map ( register_set_id INTEGER NOT NULL, \n"
     "register_id number NOT NULL, \n"
     "PRIMARY KEY (register_set_id, register_id), \n"
     "FOREIGN KEY (register_set_id) REFERENCES register_sets(register_set_id), \n"
     "FOREIGN KEY (register_id) REFERENCES registers(register_id))"  },
    

    { "device_types",  "CREATE TABLE device_types ( device_type INTEGER PRIMARY KEY, \n"
      "name VARCHAR(255) NOT NULL, \n"
      "description VARCHAR(255) NOT NULL, \n"
      "CONSTRAINT device_types_unique UNIQUE (name) )"},

    { "register_types",  "CREATE TABLE register_types ( register_type_id number generated as identity PRIMARY KEY, \n"
      "device_type INTEGER NOT NULL, \n"
      "address INTEGER NOT NULL,\n"
      "width INTEGER, \n"
      "default_value INTEGER, \n"
      "name VARCHAR(255) NOT NULL,\n"
      "description VARCHAR(255) NOT NULL,\n"
      "FOREIGN KEY (device_type) REFERENCES device_types(device_type), \n"
      "UNIQUE  ( device_type, address))"},

    { "registers", "CREATE TABLE registers ( register_id number generated as identity PRIMARY KEY, \n"
      "register_type_id INTEGER NOT NULL, \n"
      "device_key INTEGER NOT NULL, \n"
      "register INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "FOREIGN KEY (device_key) REFERENCES devices(device_key), \n"
      "FOREIGN KEY (register_type_id) REFERENCES register_types(register_type_id))" },

    { "devices", "CREATE TABLE devices ( device_key NUMBER(38,0) PRIMARY KEY, \n"
      "device_type INTEGER NOT NULL, \n"
      "ch_key INTEGER NOT NULL, \n"
      "FOREIGN KEY (ch_key) REFERENCES channels(ch_key), \n"
      "CHECK (device_key = device_type+ch_key*100), \n"
      "FOREIGN KEY (device_type) REFERENCES device_types(device_type))"},

    { "supermodules", "CREATE TABLE supermodules ( sm_id INTEGER PRIMARY KEY, barcode VARCHAR(14) NOT NULL)" },

    { "towers", "CREATE TABLE towers ( tower_key INTEGER PRIMARY KEY, \n"
      "tower_id INTEGER NOT NULL, \n"
      "sm_id INTEGER NOT NULL, \n"
      "UNIQUE (tower_id, sm_id), \n"
      "CHECK (tower_key = ((sm_id*100)+tower_id)), \n"
      "FOREIGN KEY (sm_id) REFERENCES supermodules(sm_id))"},

    { "boards", "CREATE TABLE boards ( board_key INTEGER PRIMARY KEY, \n"
      "board_type INTEGER NOT NULL, \n"
      "tower_key INTEGER NOT NULL, \n"
      "barcode INTEGER NOT NULL, \n"
      "FOREIGN KEY (tower_key) REFERENCES towers(tower_key), \n"
      "FOREIGN KEY (board_type) REFERENCES board_types(board_type))"},

    { "board_types", "CREATE TABLE board_types ( board_type INTEGER PRIMARY KEY, \n"
      "name VARCHAR(255) NOT NULL, \n"
      "description VARCHAR(255) NOT NULL )"},

    {"channels", "CREATE TABLE channels ( ch_key INTEGER PRIMARY KEY , \n"
     "ch_id INTEGER NOT NULL, \n"
     "board_key INTEGER NOT NULL, \n"
     "CHECK ((board_key*100)+ch_id = ch_key), \n"
     "FOREIGN KEY (board_key) REFERENCES boards(board_key), \n"
     "UNIQUE (ch_id, board_key))"}

  };

  const std::map<std::string, std::string> cvfetablestrs = {
    { "lpgbt_defaults", "CREATE TABLE lpgbt_defaults ("
      "device_add INTEGER NOT NULL, \n"
      "register_add INTEGER NOT NULL, \n"
      "register_val INTEGER NOT NULL, \n"
      "lpgbt_def_set_id INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"},

    { "adcs", "CREATE TABLE adcs ( \n"
      "device_key NUMBER(38,0) NOT NULL, \n"
      "set_id INTEGER NOT NULL, \n"
      "all_regs BLOB DEFAULT EMPTY_BLOB(), \n" 
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "FOREIGN KEY (device_key) REFERENCES devices(device_key), \n"
      "PRIMARY KEY (device_key, set_id))" },

    { "calibration_set_map", "CREATE TABLE calibration_set_map ( \n"
      "device_key INTEGER NOT NULL, \n"
      "set_id INTEGER NOT NULL, \n"
      "adc_set_id INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "FOREIGN KEY (device_key) REFERENCES devices(device_key), \n"
      "PRIMARY KEY (device_key, set_id, adc_set_id))" },

    { "pedestals", "CREATE TABLE pedestals ( \n"
      "ch_key INTEGER NOT NULL, \n"
      "set_id INTEGER NOT NULL, \n"
      "vref INTEGER NOT NULL, \n"
      "dac_hg INTEGER NOT NULL, \n"
      "dac_lg INTEGER NOT NULL, \n"
      "dig_ped_corr_hg INTEGER NOT NULL, \n"
      "dig_ped_corr_lg INTEGER NOT NULL, \n"
      "gain_switch_th_corr INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "FOREIGN KEY (ch_key) REFERENCES channels(ch_key), \n"
      "PRIMARY KEY (ch_key, set_id))" },

    { "pedestal_set_map", "CREATE TABLE pedestal_set_map ( \n"
      "ch_key INTEGER NOT NULL, \n"
      "set_id INTEGER NOT NULL, \n"
      "pedestal_set_id INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "FOREIGN KEY (ch_key) REFERENCES channels(ch_key), \n"
      "PRIMARY KEY (ch_key, set_id, pedestal_set_id))" },

    { "vfe_config_map", "CREATE TABLE vfe_config_map ( \n"
      "vfe_config_id INTEGER NOT NULL, \n"
      "tag VARCHAR(255), \n"
      "lpgbt_def_set_id INTEGER NOT NULL, \n"
      "dtu_def_set_id INTEGER NOT NULL, \n"
      "catia_def_set_id INTEGER NOT NULL, \n"
      "pedestal_set_id INTEGER NOT NULL, \n"
      "adc_set_id INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "PRIMARY KEY (vfe_config_id))" },

    { "catia_defaults", "CREATE TABLE catia_defaults ( \n"
      "register_add INTEGER NOT NULL, \n"
      "catia_def_set_id INTEGER NOT NULL, \n"
      "register_val INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "PRIMARY KEY (register_add, catia_def_set_id))" },

    { "dtu_defaults", "CREATE TABLE dtu_defaults ( \n"
      "register_add INTEGER NOT NULL, \n"
      "dtu_def_set_id INTEGER NOT NULL, \n"
      "register_val INTEGER NOT NULL, \n"
      "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \n"
      "PRIMARY KEY (register_add, dtu_def_set_id))" }
  };

  const std::map<std::string, std::string> cvfeseqstrs = {
    { "lpgbt_defaults_seq", "CREATE SEQUENCE lpgbt_defaults_seq INCREMENT BY 1"},
    { "adcs_seq", "CREATE SEQUENCE adcs_seq INCREMENT BY 1"},
    { "adc_full_set_seq", "CREATE SEQUENCE adc_full_set_seq INCREMENT BY 1"},
    { "pedestals_seq", "CREATE SEQUENCE pedestals_seq INCREMENT BY 1"},
    { "ped_full_set_seq", "CREATE SEQUENCE ped_full_set_seq INCREMENT BY 1"},
    { "catia_defaults_seq", "CREATE SEQUENCE catia_defaults_seq INCREMENT BY 1"},
    { "dtu_defaults_seq", "CREATE SEQUENCE dtu_defaults_seq INCREMENT BY 1"},
    { "vfe_config_map_seq", "CREATE SEQUENCE vfe_config_map_seq INCREMENT BY 1"}
  };

  const std::map<std::string, std::string> dropvfetablestrs = {
    {"dvfe_config_map", "drop table vfe_config_map"},
    {"dcalibration_set_map",  "drop table calibration_set_map"},
    {"dadcs", "drop table adcs"},
    {"dpedestal_set_map", "drop table pedestal_set_map"},
    {"dpedestals", "drop table pedestals"},
    {"dcatia_defaults", "drop table catia_defaults"},
    {"ddtu_defaults", "drop table dtu_defaults"},
    {"dlpgbt_defaults", "drop table lpgbt_defaults"}
  };

  const std::map<std::string, std::string> droptablestrs = {
    {"dbitmaps",  "drop table bitmaps"},
    {"dregister_set_map","drop table register_set_map"},
    {"dregister_sets","drop table register_sets"},
    {"dregisters","drop table registers"},
    {"dregister_types","drop table register_types"},
    {"ddevices","drop table devices"},
    {"ddevice_types","drop table device_types"},
    {"dchannels","drop table channels"},
    {"dboards","drop table boards"},
    {"dboard_types","drop table board_types"},
    {"dtowers","drop table towers"}

  };

  const std::map<std::string, std::string> cviewstrs = {
    {"channel_map", "CREATE VIEW channel_map AS \n"
     "select \n"
     "sm.sm_id as sm_id, \n"
     "tw.tower_id as tower_nb, \n"
     "br.board_type as vfe_nb, \n"
     "ch.ch_id as ch_nb, \n"
     "ch.ch_key as ch_key, \n"
     "5*br.board_type+ch.ch_id as chintw_nb \n"
     "from supermodules sm, towers tw, boards br, channels ch where \n"
     "ch.ch_key=ch.board_key*100+ch_id and \n"
     "ch.board_key=br.board_key and \n"
     "br.tower_key=tw.tower_key and \n"
     "br.board_type<5 \n"
        //bcpdb_oracle(const std::string &userName, const std::string &password, const std::string &connectString, bool create);
     "order by sm.sm_id asc, tw.tower_id, br.board_type, ch.ch_id; \n"}
  };  
  const std::vector<std::string> vieworder = { "channel_map" };

  const std::map<std::string, std::string> insertstrs = {
    {"insert_device_type", "INSERT INTO device_types ( device_type, name, description ) \n"
     "VALUES ( :1, :2, :3 )"},
    {"insert_board_type", "INSERT INTO board_types ( board_type, name, description ) \n"
     "VALUES ( :1, :2, :3 )"},
    {"insert_device", "INSERT INTO devices ( device_key, device_type, ch_key ) \n"
     "VALUES ( :1, :2, :3 )"},
    {"insert_register_type", "INSERT INTO register_types ( device_type, address, width, default_value,\n"
     "name, description) VALUES (:1, :2, :3, :4, :5, :6)"},
    {"insert_register", "INSERT INTO registers (register_type_id, device_key, register) VALUES (:1, :2, :3) \n"
    "RETURNING register_id INTO :4"},
    {"insert_bitmap", "INSERT INTO bitmaps ( address, register_type_id, name, description, mask, access) \n"
    "VALUES (:1, :2, :3, :4, :5, :6)"},
    {"insert_supermodule", "INSERT INTO supermodules (sm_id) VALUES (:1)"},
    {"insert_tower", "INSERT INTO towers (tower_key, tower_id, sm_id) VALUES (:1, :2, :3)"},
    {"insert_board", "INSERT INTO boards (board_key, board_type, tower_key, barcode) VALUES (:1, :2, :3, :4)"},
    
    {"insert_channel", "INSERT INTO channels (ch_key, ch_id, board_key) VALUES (:1, :2, :3)"},
    {"insert_register_set","INSERT INTO register_sets (notes) VALUES (:1) RETURNING register_set_id INTO :2"},
//     {"insert_register_set","INSERT INTO register_sets (notes) VALUES (:1) RETURNING register_set_id"},
    {"insert_set_map_entry","INSERT INTO register_set_map (register_set_id, register_id) VALUES (:1, :2)"},

     // New VFE tables
    {"insert_adc_first","INSERT INTO adcs (device_key, set_id) VALUES (:1,:2)"},
    {"insert_adc_second", "SELECT device_key, all_regs FROM adcs where set_id=:1 FOR UPDATE"},
    {"insert_calibration_set","INSERT INTO calibration_set_map (device_key, set_id, adc_set_id ) VALUES (:1,:2,:3)"},
    {"insert_pedestal","INSERT INTO pedestals \n"
     "(ch_key, set_id, vref, dac_hg, dac_lg, dig_ped_corr_hg, dig_ped_corr_lg, gain_switch_th_corr) VALUES (:1,:2,:3,:4,:5,:6,:7,:8)"},
    {"insert_pedestal_set","INSERT INTO pedestal_set_map (ch_key, set_id, pedestal_set_id ) VALUES (:1,:2,:3)"},
    {"insert_vfe_map","INSERT INTO vfe_config_map (vfe_config_id, tag, lpgbt_def_set_id, dtu_def_set_id, catia_def_set_id, pedestal_set_id, adc_set_id) \n"
     " VALUES (:1,:2,:3,:4,:5,:6,:7)"},
    {"insert_catia_def","INSERT INTO catia_defaults (register_add, catia_def_set_id, register_val) VALUES (:1,:2,:3)"},
    {"insert_dtu_def","INSERT INTO dtu_defaults (register_add, dtu_def_set_id, register_val) VALUES (:1,:2,:3)"},
    {"insert_lpgbt_def","INSERT INTO lpgbt_defaults (device_add, register_add, register_val, lpgbt_def_set_id) VALUES (:1,:2,:3,:4)"}
  };

  const std::map<std::string, std::string> selectstrs = {
    {"getSMs", "SELECT sm_id FROM supermodules;"},
    {"getTowers", "SELECT tower_key, tower_id FROM towers;"},
    {"getDeviceTypes", "SELECT * FROM device_types;"},
    {"getDevices", "SELECT * FROM devices;"},
    {"getBoards", "SELECT * FROM boards;"},
    {"getBoardChannels", "SELECT ch_key FROM channels JOIN boards USING (board_key) JOIN Towers \n"
     "USING (tower_key) JOIN Supermodules USING (sm_id) WHERE (sm_id = :1 AND \n"
     "Towers.tower_id = :2 AND boards.board_type = :3)"},
    /* Notice: when selection with WHERE after a JOIN,
     * if the column was the same use for the JOIN you
     * you do not need to specify the table, because it
     * is in both (e.g. sm_id = 0 **NOT* Tower.sm_id = 0)
     */
    {"register_types", "SELECT * FROM register_types"},
    {"channels", "SELECT ch_key, ch_id FROM channels;"},
    {"tower_channels", "SELECT ch_key,ch_id FROM channels JOIN boards USING (board_key) \n"
     "JOIN Towers USING (tower_key) JOIN Supermodules USING (sm_id) WHERE Towers.tower_id = ?1 AND \n"
     "Towers.sm_id = ?2 AND board_type = ?3;"},
    {"channel_devices", "SELECT device_key,device_type FROM devices WHERE (ch_key = :1)"},
    {"getDeviceFromTypeAndKey", "SELECT device_key FROM devices WHERE (ch_key = :1 AND device_type = :2)"},
    {"device_registers", "SELECT r.register, rtypes.name, rtypes.address FROM registers r \n"
     "JOIN register_types rtypes \n"
     "USING (register_type_id) WHERE (:1 = r.device_key) ORDER BY rtypes.address"},
    {"getAdcs", "SELECT set_id, device_key, all_regs FROM adcs"},
    {"getAdcsByFullSetId", "SELECT a.set_id, a.device_key, a.all_regs FROM adcs a \n"
     "LEFT JOIN calibration_set_map b ON a.set_id=b.set_id and a.device_key=b.device_key WHERE (b.adc_set_id=:1) ORDER BY a.device_key"},
    {"getAdcsByFullSetIdAndDeviceKeyRange", "SELECT a.set_id, a.device_key, a.all_regs FROM adcs a \n"
     "LEFT JOIN calibration_set_map b ON a.set_id=b.set_id and a.device_key=b.device_key WHERE (b.adc_set_id=:1) and (b.device_key>:2 and b.device_key<:3) ORDER BY a.device_key"},
    {"getPedsByFullSetId", "SELECT a.set_id, a.ch_key, a.vref, a.dac_hg, a.dac_lg, a.dig_ped_corr_hg, a.dig_ped_corr_lg, a.gain_switch_th_corr \n"
     " FROM pedestals a LEFT JOIN pedestal_set_map b ON a.set_id=b.set_id and a.ch_key=b.ch_key WHERE (b.pedestal_set_id=:1)"},
    {"getCatiaDefBySetId", "SELECT catia_def_set_id, register_add, register_val FROM catia_defaults WHERE (catia_def_set_id=:1) ORDER BY register_add"},
    {"getDtuDefBySetId", "SELECT dtu_def_set_id, register_add, register_val FROM dtu_defaults WHERE (dtu_def_set_id=:1) ORDER BY register_add"},
    {"getLpgbtDefBySetId", "SELECT lpgbt_def_set_id, device_add, register_add, register_val FROM lpgbt_defaults WHERE (lpgbt_def_set_id=:1) ORDER BY device_add, register_add"},
    {"getFullVFEConfigById", "SELECT vfe_config_id, tag, lpgbt_def_set_id, dtu_def_set_id, catia_def_set_id, pedestal_set_id, adc_set_id \n"
     " FROM vfe_config_map WHERE vfe_config_id=:1"}
  };

  class bcpdb_oracle : public bcpdb {
  public: 
    bcpdb_oracle();
    ~bcpdb_oracle();
    bool close();

    bcpdb_oracle(const std::string &userName, const std::string &password, const std::string &connectString, bool create);
    bcpdb_oracle(std::string userName, std::string password, std::string connectString); 
    bcpdb_oracle(log4cplus::Logger* logger, std::string userName, std::string password, std::string connectString);
    bool create_tables();
    void create_table(const std::string &ctstmt);
    void dropAllTables();
    bool memoryMode();
    bool prepare_statements();
    void prepare_statement(const std::string &key, const std::string &cmd);
    
    bool createVFETables();
    void dropVFETables();
    void createVFESequences();
    void dropVFESequences();
    int getNextVal(std::string sequence_name);

    int getLatestVFEConfigId();
    int getLatestLpgbtDefSetId();

    int getTowerFromDeviceKey(long int device_key);
    int getBoardFromDeviceKey(long int device_key);
    int getChannelFromDeviceKey(long int device_key);
    int getDeviceTypeFromDeviceKey(long int device_key);
    int getTowerChNumFromDeviceKey(long int device_key);
    int getTowerFromChKey(int channel_key);
    int getBoardFromChKey(int channel_key);
    int getTowerChNumFromChKey(int channel_key);

    // VFE functions
    json addAdcs(json& electronics_json);
    int addCalibrationSet(json& adc_set_ids);
    json addPedestals(json& electronics_json);
    int addPedestalSet(json& ped_set_ids);
    int addCatiaDefaults(json& electronics_json);
    int addDtuDefaults(json& electronics_json);
    int addLpgbtDefaults(std::map<unsigned int, std::map<unsigned int, unsigned int> > full_reg_map);
    json getVFEConfig(int vfe_config_id);
    void getAdcs(int full_calib_set_id, json &full_config);
    void getPedestals(int full_ped_set_id, json &full_config);
    void getCatiaDefaults(int catia_def_set_id, json &full_config);
    void getDtuDefaults(int dtu_def_set_id, json &full_config);
    void getLpgbtDefaults(int lpgbt_def_set_id, json &full_config);

    int addVFEConfig(std::string tag, int full_calib_set_id, int full_ped_set_id, int dtu_def_set_id, int catia_def_set_id, int lpgbt_def_set_id);


    void addBoardType(int id, std::string &name, std::string &desc);
    void addBoardTypes(std::map<int, std::pair<std::string, std::string>> &board_types);    

    void addDeviceTypes(std::map<int, std::pair<std::string, std::string>> &device_types);
    void addSuperModule (unsigned int id);
    int addTowers();
    int addBoards();
    int addChannels();
    int addDevices();
    int addRegister(Register &r);
    void addRegisterType(Register_Type &r);
    int addRegisterSet();
    int addRegisterSetMap(int set_id, std::vector<int> reg_id_vector);
    /// selectors
    std::vector<int> getBoardChannels(int board_type, int tower_id, int supermodule);
    std::vector<Device> devices(int channel_id);
    int getDeviceFromTypeAndKey(int ch_key, device device_type);
    std::vector<Register_Type> get_register_types();
    std::vector<Register> registers(int device_id);
  private:

    log4cplus::Logger* logger_;    
    Environment *env;
    Connection *conn;
    std::map<std::string, Statement*> _statements;
    std::set<std::string> expectedTables; 

  };



  
  }

}

#endif 
