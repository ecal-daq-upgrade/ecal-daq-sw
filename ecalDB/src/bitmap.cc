#include "bitmap.h"
#include <iostream> 

bitmap::bitmap() {
    name = "empty";
    description = "No Description";
}

bitmap::bitmap(size_t sz) {
  name = "empty";
  description = "No Description";
  bits.resize(sz);
  pon_reset_value.resize(sz); 
  bits.reset();
  pon_reset_value.reset();

}; 


void bitmap::print_bits() {
  
  for (int i = 0; i < bits.size(); i++) {
    std::cout << bits[i] << "\t";
  }
  std::cout << std::endl; 

}

// meh, this *does* work, but will rapidly become unusable
// meaning I'll have to move the fn defs into the header file 
//template class bitmap<8>; 
