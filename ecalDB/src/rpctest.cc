#include <wiscrpcsvc.h>
#include <nlohmann/json.hpp>
#include <rpctest.h>

using namespace wisc;
using json = nlohmann::json;

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)


/*int testRpcConnection(std::string hostname) {

  RPCSvc rpc; 
    try {
        rpc.connect(hostname);
        return 0;
  } catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
  } catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
  }

}*/

void openRpcConnection(RPCSvc& rpc, std::string hostname) {
  rpc.connect(hostname);
}

/*json readReagister(log4cplus::Logger logger, std::string hostname, std::string towers) {

  RPCSvc rpc;
  openRpcConnection(rpc, hostname);

  RPCMsg req, rsp;
 
  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;

  req = RPCMsg("fwmodule.getAllVFERegisters");
  rsp = RPCMsg();



  req.set_string("towers", towers);
  rsp = rpc.call_method(req);
  std::string jsonstring = rsp.get_string("registers");
  json fullconfig = json::parse(jsonstring);
  return fullconfig;
}*/

int getallregisters(log4cplus::Logger logger, RPCSvc &rpc, std::string towers, std::string &jsonresponse) {

  RPCMsg req, rsp;

  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;
  req = RPCMsg("fwmodule.getAllVFERegisters");
  rsp = RPCMsg();

  req.set_string("towers", towers);
  rsp = rpc.call_method(req);
  jsonresponse = rsp.get_string("registers");
  return 0;
}


int configureFesFromSingleFile(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers) {

  RPCMsg req, rsp;

  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;
  req = RPCMsg("fwmodule.configurefesfromsinglefile");
  rsp = RPCMsg();
  try {
    req.set_string("configjsonstring", configjsonstring);
    req.set_string("towers", towers);
    rsp = rpc.call_method(req);
  }
  STANDARD_CATCH;
  return 0;
}

int resetVfes(log4cplus::Logger logger, RPCSvc &rpc, std::string towers) {
  RPCMsg req, rsp;
  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;
  req = RPCMsg("fwmodule.fastresetvfe");
  rsp = RPCMsg();
  try {
    req.set_string("towers", towers);
    rsp = rpc.call_method(req);
  }
  STANDARD_CATCH;
  return 0;
}

int configureVfesFromSingleFile(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers) {

  RPCMsg req, rsp;

  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;
  req = RPCMsg("fwmodule.configurevfesfromsinglefile");
  rsp = RPCMsg();
  try {
    req.set_string("configjsonstring", configjsonstring);
    req.set_string("towers", towers);
    rsp = rpc.call_method(req);
  }
  STANDARD_CATCH;
  return 0;
}


int prepareDTUAlign(log4cplus::Logger logger, RPCSvc &rpc, std::string configjsonstring, std::string towers) {
  RPCMsg req, rsp;
  try {
      ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
  }
  STANDARD_CATCH;
  req = RPCMsg("fwmodule.prepareDTUalign");
  rsp = RPCMsg();
  try {
    req.set_string("configjsonstring", configjsonstring);
    req.set_string("towers", towers);
    rsp = rpc.call_method(req);
  }
  STANDARD_CATCH;
  return 0;
}
