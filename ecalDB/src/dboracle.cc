#include "dboracle.h"
#include <nlohmann/json.hpp>
// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"
using namespace oracle::occi;
using json = nlohmann::json;

using namespace log4cplus;
namespace bcp {
  namespace myoracle  { 

    bcpdb_oracle::bcpdb_oracle() {
      _type = Oracle;
    
    }

    bcpdb_oracle::~bcpdb_oracle() { }; 

    //#########################################################
    bcpdb_oracle::bcpdb_oracle(const std::string &userName, const std::string &password, const std::string &connectString, bool create=false) { //FIXME args will be userName, pw, string
      _type = Oracle;
      // In the constructor we open the DB connection in any case
      std::cout << "Opening database" << std::endl;
      env = Environment::createEnvironment(Environment::DEFAULT);
      conn = env->createConnection(userName, password, connectString);       
      if (conn == nullptr) throw std::runtime_error("Failed to open db");

      if (create) {
        if (!create_tables()) 
	    throw std::runtime_error("Failed to create dbs");
      }    
    }

    bcpdb_oracle::bcpdb_oracle(std::string userName, std::string password, std::string connectString) { //FIXME args will be userName, pw, string
      _type = Oracle;
      // In the constructor we open the DB connection in any case
      std::cout << "Opening database" << std::endl;
      env = Environment::createEnvironment(Environment::DEFAULT);
      conn = env->createConnection(userName, password, connectString);
      if (conn == nullptr) throw std::runtime_error("Failed to open db");

    }

    bcpdb_oracle::bcpdb_oracle(log4cplus::Logger* logger, std::string userName, std::string password, std::string connectString) { //FIXME args will be userName, pw, string
      _type = Oracle;
      logger_ = logger;
      // In the constructor we open the DB connection in any case
      std::cout << "Opening database" << std::endl;
      env = Environment::createEnvironment(Environment::DEFAULT);
      conn = env->createConnection(userName, password, connectString);
      if (conn == nullptr) throw std::runtime_error("Failed to open db");

    }

    int bcpdb_oracle::getTowerFromDeviceKey(long int device_key) {
      return (device_key/100/100/100)%100;
    }
    int bcpdb_oracle::getBoardFromDeviceKey(long int device_key) {
      return (device_key/100/100)%100;
    }
    int bcpdb_oracle::getChannelFromDeviceKey(long int device_key) {
      return (device_key/100)%100;
    }
    int bcpdb_oracle::getDeviceTypeFromDeviceKey(long int device_key) {
      return (device_key)%100;
    }
    int bcpdb_oracle::getTowerChNumFromDeviceKey(long int device_key) {
      return getBoardFromDeviceKey(device_key) * 5 + getChannelFromDeviceKey(device_key);
    }
    int bcpdb_oracle::getTowerFromChKey(int channel_key) {
      return (channel_key/100/100)%100;
    }
    int bcpdb_oracle::getBoardFromChKey(int channel_key) {
      return (channel_key/100)%100;
    }
    int bcpdb_oracle::getTowerChNumFromChKey(int channel_key) {
      return getBoardFromChKey(channel_key) * 5 + channel_key%100;
    }

    //#########################################################
    void bcpdb_oracle::create_table(const std::string &ctstmt) { // this ctstmt is the CREATE TABLE ... sql syntax
      try {
        Statement *stmt = conn->createStatement(ctstmt);
        stmt->executeUpdate();
        conn->terminateStatement(stmt);
        std::cout << "Table created successfully." << std::endl;
      } catch(SQLException ex) { 
        // for the moment we don't block the script for an error on table creation... 
        // Maybe the table already exist...
        if (ex.getErrorCode() == 955) std::cout << "Table already exists. Skipping..." << std::endl;
        else {
          std::cout << "Exception thrown" << std::endl;
          std::cout << "Error number: "   << ex.getErrorCode() << std::endl;
          std::cout << ex.getMessage()    << std::endl;
          throw ex;
        }
      }
    }
   
    // ################################## 
    bool bcpdb_oracle::create_tables() {

      std::cout << "Creating tables" << std::endl; 
      try { 
        for (auto &key : bcp::tableorder) {
	  
	        const std::string createstmt = ctablestrs.at(key); // what is this???
	        std::cout << "Creating " << key << " table." << std::endl;
	        std::cout << createstmt << std::endl; 
	        create_table(createstmt);
          std::cout << "\n\n" << std::endl; 
	      }
      } catch (std::runtime_error &err) {
	      std::cout << "Error creating tables: " << err.what() << std::endl; 
	      return false;
      }
      return true; 
    }

    // ##################################
    void bcpdb_oracle::dropAllTables() {
      std::cout << "Dropping tables" << std::endl;
      for (auto i = bcp::tableorder.rbegin(); i != bcp::tableorder.rend(); ++i) {
        std::cout << "Dropping table " << *i << std::endl;
        std::string check = *i;
        if (check.find("supermodule") != std::string::npos) {
          std::cout << "Skipping supermodules table" << std::endl;
          continue;
        }
        const std::string dropstmt = droptablestrs.at("d"+*i); // Adding the d for the drop table keys
        std::cout << dropstmt << std::endl;
        try {
          Statement *statement = conn->createStatement(dropstmt);
          statement->executeUpdate();
          conn->terminateStatement(statement);
        } catch(SQLException ex) {
          std::cout<<"Exception thrown for dropAllTables"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
        }
      }
    }

    // ###################################
    bool bcpdb_oracle::createVFETables() {
      std::cout << "Creating tables" << std::endl;
      try {
        for (auto &key : bcp::vfetableorder) {

          const std::string createstmt = cvfetablestrs.at(key); // what is this???
          std::cout << "Creating " << key << " table." << std::endl;
          std::cout << createstmt << std::endl;
          create_table(createstmt);
          std::cout << "\n\n" << std::endl;
        }
      } catch (std::runtime_error &err) {
          std::cout << "Error creating tables: " << err.what() << std::endl;
          return false;
      }
      return true;
    }

    // ##################################
    void bcpdb_oracle::dropVFETables() {
      std::cout << "Dropping VFE tables" << std::endl;
      for (auto i = bcp::vfetableorder.rbegin(); i != bcp::vfetableorder.rend(); ++i) {
        std::cout << "Dropping table " << *i << std::endl;
        std::string drop_name = "d"+*i;
        const std::string dropstmt = dropvfetablestrs.at(drop_name); // Adding the d for the drop table keys
        std::cout << dropstmt << std::endl;
        try {
          Statement *statement = conn->createStatement(dropstmt);
          statement->executeUpdate();
          conn->terminateStatement(statement);
        } catch(SQLException ex) {
          std::cout<<"Exception thrown for dropVFETables"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
        }
      }
    }
    // ###############################
    void bcpdb_oracle::createVFESequences() {
      std::cout << "Creating VFE sequences" << std::endl;
      for (auto& item: cvfeseqstrs) {
        try {

          std::cout << "Creating " << item.first << " sequence." << std::endl;
          std::cout << item.second << std::endl;
          Statement *statement = conn->createStatement(item.second);
          statement->executeUpdate();
          conn->terminateStatement(statement);
        } catch (std::runtime_error &err) {
          std::cout << "Error creating sequences: " << err.what() << std::endl;
        } catch(SQLException ex) {
          if (ex.getErrorCode() == 955) std::cout << "Sequence already exists. Skipping..." << std::endl;
          else std::cout<<ex.getMessage() << std::endl;
        }
      }
    }
    // ###############################

    void bcpdb_oracle::dropVFESequences() {
      std::cout << "Dropping VFE sequences" << std::endl;
      try {
        for (auto& item: cvfeseqstrs) {

          std::cout << "Dropping " << item.first << " sequence." << std::endl;
          Statement *statement = conn->createStatement("DROP SEQUENCE "+ item.first);
          statement->executeUpdate();
          conn->terminateStatement(statement);
        }
      } catch (std::runtime_error &err) {
          std::cout << "Error creating sequences: " << err.what() << std::endl;
      }
    }
    
    // ###############################
    json bcpdb_oracle::addAdcs(json& electronics_json) {
      int sm_id = 36;

      int set_id = getNextVal("adcs_seq");
      std::cout<<"Generated set_id for adcs insert: "<<set_id<<std::endl;

      // insert_adc --> "INSERT INTO adcs (device_key, set_id, all_regs) VALUES (:1,:2,:3)"
      Statement *statement = _statements["insert_adc_first"];


      // We need to know how many iterations we will do with oracle.
      // Is there a better way to count all the registers we will write?
      int maxIterations = 0;
      for (auto &twrs : electronics_json["towers"].items()) {
        auto &twr = electronics_json["towers"][twrs.key()];
        for (auto &chs : twr["channels"].items()) {
          auto &ch = twr["channels"][chs.key()];
          for (auto &device : ch.items()) { 
            if (device.key() == "adc0" || device.key() == "adc1") {
                maxIterations ++;
            }
          }
        }
      }
      std::cout << "iterations " << maxIterations << std::endl;
      statement->setMaxIterations(maxIterations);

      // Here we create the json to pass to the function that will fill the calibration_set_map
      int iterations = 0;

      for (auto &twrs : electronics_json["towers"].items()) {
        auto &twr = electronics_json["towers"][twrs.key()];
        int tower_id = std::stoul(twrs.key());
        for (auto &chs : twr["channels"].items()) {
          auto &ch = twr["channels"][chs.key()];
          int channel = std::stoul(chs.key());
          // int(channel/5) is practically the board number 0=VFE0, 1=VFE1 ...
          unsigned int ch_key = ((sm_id*100 + tower_id)*100 + int(channel/5))*100 + int(channel)%5;
          for (auto &device : ch.items()) {
            int device_id = 0;
            if      (device.key() == "adc0")  device_id = 0;
            else if (device.key() == "adc1")  device_id = 1;
            else continue;
            long int device_key = ch_key*100 + device_id;
            if (device.value().size() == 0) continue;
            if (iterations != 0) statement->addIteration();
            Number deviceKeyNumber(device_key);
            statement->setNumber(1, deviceKeyNumber);
            statement->setInt(2, set_id);
            iterations ++;
          }
        }
      }
      std::cout << "\ninsert_adc_first end\n" << std::endl;


      statement->executeUpdate();
      conn->terminateStatement(statement);
      conn->commit();
      
      std::cout << "\nNow we insert the blobs\n" << std::endl;
      json json_for_calib_set_map;

      // "insert_adc_second" --> "SELECT device_key, all_regs FROM adcs where set_id=:1 FOR UPDATE"
      Statement *stmt = _statements["insert_adc_second"];
      stmt->setInt(1, set_id);
      ResultSet *rset=stmt->executeQuery();
      while (rset->next()) {
        
        long int device_key = rset->getNumber(1);
        std::cout<< "Device key " << device_key<< std::endl;
        int tower = getTowerFromDeviceKey(device_key);
        int channel = getTowerChNumFromDeviceKey(device_key);
        int device_type = getDeviceTypeFromDeviceKey(device_key);
        std::string adc = "adc" + std::to_string(device_type);
        auto device = electronics_json["towers"][std::to_string(tower)]["channels"][std::to_string(channel)][adc];
        
        int counter = 0;
        unsigned char* all_regs = new unsigned char[76]();

        for (auto reg : device) {
          int reg_int = reg;
          all_regs[counter] = (unsigned char)reg_int;
          counter ++;
        }
        if (counter == 0) continue;
        Blob blob=rset->getBlob(2);
        if(blob.isNull())  {
          std::cout<< "NULL Blob";
          continue;
        } else {
          blob.open(OCCI_LOB_READWRITE);
          blob.write(76, all_regs, 76);
          blob.close();
          json_for_calib_set_map[std::to_string(device_key)] = set_id;
        }
      }
      
      stmt->closeResultSet(rset);
      conn->commit();
      // Method to write blobs taken from:
      // https://docs.oracle.com/en/database/oracle/oracle-database/18/lncpp/programming-with-lobs.html#GUID-35089C36-3FF6-44E8-85F6-16228FF81E02
     
      return json_for_calib_set_map;
    } 

    int bcpdb_oracle::addCalibrationSet(json& adc_set_ids) {
      int adc_set_id = getNextVal("adc_full_set_seq");
      int json_length = adc_set_ids.size();

      std::cout<<"Generated adc_set_id for calibration_set_map insert: "<<adc_set_id<<std::endl; 

      // insert_calibration_set --> "INSERT INTO calibration_set_map (device_key, set_id, adc_set_id ) VALUES (:1,:2,:3)"
      Statement *statement = _statements["insert_calibration_set"];
      statement->setMaxIterations(json_length);


      int iterations = 0;
      std::cout << "json_length " << json_length << std::endl;
      for (auto& el : adc_set_ids.items()) {
        Number deviceKeyNumber(stoul(el.key()));
        statement->setNumber(1, deviceKeyNumber);
        statement->setInt(2, el.value());
        statement->setInt(3, adc_set_id);
        iterations++;
        if (iterations < json_length) statement->addIteration();
      }
      statement->executeUpdate();
      conn->terminateStatement(statement);

      return adc_set_id;
    }

    // ###############################
    json bcpdb_oracle::addPedestals(json& electronics_json) {
      int sm_id = 36;

      int set_id = getNextVal("pedestals_seq");
      std::cout<<"Generated set_id for pedestals insert: "<<set_id<<std::endl;

      // insert_pedestal --> insert_pedestal","INSERT INTO pedestals \n"
      //      "(ch_key, set_id, vref, dac_hg, dac_lg, dig_ped_corr_hg, dig_ped_corr_lg, gain_switch_th_corr) VALUES (:1,:2,:3,:4,:5,:6,:7,:8)"
      Statement *statement = _statements["insert_pedestal"];


      // We need to know how many iterations we will do with oracle.
      // Is there a better way to count all the channels we will write?
      int maxIterations = 0;
      for (auto &twrs : electronics_json["towers"].items()) {
        auto &twr = electronics_json["towers"][twrs.key()];
        for (auto &chs : twr["channels"].items()) {
          maxIterations ++;
        }
      }
      std::cout << "max iterations " << maxIterations << std::endl;
      statement->setMaxIterations(maxIterations);

      json json_for_ped_set_map;
      int iterations = 0;
      for (auto &twrs : electronics_json["towers"].items()) {
        auto &twr = electronics_json["towers"][twrs.key()];
        int tower_id = std::stoul(twrs.key());
        for (auto &chs : twr["channels"].items()) {
          auto &ch = twr["channels"][chs.key()];
          int channel = std::stoul(chs.key());
          unsigned int ch_key = ((sm_id*100 + tower_id)*100 + int(channel/5))*100 + int(channel)%5;
          auto &dtu = ch["dtu"];
          auto &catia = ch["catia"];
          if (ch["dtu"].size() == 0 || ch["catia"].size() == 0) continue;
          if (iterations != 0) statement->addIteration();
          std::cout<<iterations<<" "<<ch["dtu"].size() << " "<<ch["catia"].size()<<std::endl;
          int vref = catia[0x6];
          vref >>= 4;
          int reg3 = catia[0x3]; // it is a 2 bytes reg
          int dac_lg = (reg3 >> 8) & 0x3f;
          int dac_hg = (reg3 & 0xff) >> 2;
          int dig_ped_corr_lg = dtu[0x6];
          int dig_ped_corr_hg = dtu[0x5];
          int reg7 = dtu[0x7];
          int reg8 = dtu[0x8];
          int gain_switch_th_corr = reg7+((reg8 & 0xf) << 8);

          //std::cout<<ch_key<<" - Ped vref "<<vref<<" dac_lg "<<dac_lg<<" dac_hg "<<dac_hg<<" dig_ped_corr_lg "<<dig_ped_corr_lg<<" dig_ped_corr_hg "<<dig_ped_corr_hg<<" gain_switch_th_corr "<<gain_switch_th_corr<<std::endl;

          statement->setInt(1, ch_key);
          statement->setInt(2, set_id);
          statement->setInt(3, vref);
          statement->setInt(4, dac_hg);
          statement->setInt(5, dac_lg);
          statement->setInt(6, dig_ped_corr_hg);
          statement->setInt(7, dig_ped_corr_lg);
          statement->setInt(8, gain_switch_th_corr);
          json_for_ped_set_map[std::to_string(ch_key)] = set_id;
          
          iterations ++;
          //if (iterations < maxIterations) statement->addIteration(); 
        }
      }
      std::cout << "ready to execute all pedestals iterations" << std::endl;
      statement->executeUpdate();
      conn->terminateStatement(statement);
      std::cout << "executed all pedestals iterations" << std::endl;

      return json_for_ped_set_map;
    }
   
    int bcpdb_oracle::addPedestalSet(json& ped_set_ids) {
      int pedestal_set_id = getNextVal("ped_full_set_seq");
      int json_length = ped_set_ids.size();

      std::cout<<"Generated pedestal_set_id for pedestal_set_map insert: "<<pedestal_set_id<<std::endl;

      // insert_calibration_set --> "INSERT INTO pedestal_set_map (ch_key, set_id, pedestal_set_id ) VALUES (:1,:2,:3)"
      Statement *statement = _statements["insert_pedestal_set"];
      statement->setMaxIterations(json_length);

      int iterations = 0;
      std::cout << "json_length " << json_length << std::endl;
      for (auto& el : ped_set_ids.items()) {
        statement->setInt(1, stoul(el.key()));
        statement->setInt(2, el.value());
        statement->setInt(3, pedestal_set_id);
        iterations++;
        if (iterations < json_length) statement->addIteration();
      }
      statement->executeUpdate();
      conn->terminateStatement(statement);

      return pedestal_set_id;
    }

    int bcpdb_oracle::addLpgbtDefaults(std::map<unsigned int, std::map<unsigned int,unsigned int> > full_reg_map) {

      int set_id = getNextVal("lpgbt_defaults_seq");
      std::cout<<"Generated set_id for lpgbt_defaults insert: "<<set_id<<std::endl;
      Statement *statement = _statements["insert_lpgbt_def"];
      // First we count how many regs we need to insert

      int num_max_queries = 0;
      for (auto const& lpgbt : full_reg_map) {   
        for (auto const& reg : lpgbt.second) {
          unsigned int reg_add = reg.first;
          unsigned int reg_val = reg.second;
          unsigned int lpgbt_add = lpgbt.first;
          std::cout<<lpgbt_add<<" "<<reg_add << " " << reg_val<<std::endl;
          num_max_queries ++;
        }
      }
      std::cout << "iterations " << num_max_queries << std::endl;
      statement->setMaxIterations(num_max_queries);
      
      try {
        int counter = 0;
        for (auto const& lpgbt : full_reg_map) {
          for (auto const& reg : lpgbt.second) {
            unsigned int reg_add = reg.first;
            unsigned int reg_val = reg.second;
            unsigned int lpgbt_add = lpgbt.first;
            statement->setInt(1, lpgbt_add);
            statement->setInt(2, reg_add);
            statement->setInt(3, reg_val);
            statement->setInt(4, set_id);
            if (counter < num_max_queries-1) statement->addIteration();
            counter++;
          }
        }
        statement->executeUpdate();
        conn->terminateStatement(statement);
      } catch(SQLException ex) {
          std::cout<<"Exception thrown for addCatiaDefaults"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
      }

      return set_id;
    }

    int bcpdb_oracle::addCatiaDefaults(json& electronics_json) {
      int set_id = getNextVal("catia_defaults_seq");
      std::cout<<"Generated set_id for catia_defaults insert: "<<set_id<<std::endl;
      // insert catia defaults --> insert_catia_def","INSERT INTO catia_defaults \n"
      //      "(register_add, set_id, register_val) VALUES (:1,:2,:3)"
      Statement *statement = _statements["insert_catia_def"];


      // We need to know how many iterations we will do with oracle.
      // Is there a better way to count all the channels we will write?
      std::vector<int> reg_values;
      bool defaults_done = false;
      for (auto &twrs : electronics_json["towers"].items()) {
        if (defaults_done) break;
        auto &twr = electronics_json["towers"][twrs.key()];
        for (auto &chs : twr["channels"].items()) {
          if (defaults_done) break;
          auto &ch = twr["channels"][chs.key()];
          int channel = std::stoul(chs.key());
          // We use first channel not empty to get the default values of CATIA chips
          if (ch["catia"].size() == 0) continue;
          auto &catia = ch["catia"];
          for (auto &val : catia) {
                std::cout<<"catia "<<val<<std::endl;
                reg_values.push_back(val);
          }
          defaults_done = true;
        }
      }

      std::cout << "iterations " << reg_values.size() << std::endl;
      statement->setMaxIterations(reg_values.size());

      try {
        int counter = 0;
        for (auto val: reg_values) {
            statement->setInt(1, counter);
            statement->setInt(2, set_id);
            statement->setInt(3, val);
            std::cout<< counter << " "<<reg_values.size()<<std::endl;
            if (counter < reg_values.size()-1) statement->addIteration();
            counter++;
        }
        statement->executeUpdate();
        conn->terminateStatement(statement);
      } catch(SQLException ex) {
          std::cout<<"Exception thrown for addCatiaDefaults"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
      }
      return set_id;
    }

    int bcpdb_oracle::addDtuDefaults(json& electronics_json) {
      int set_id = getNextVal("dtu_defaults_seq");
      std::cout<<"Generated set_id for dtu_defaults insert: "<<set_id<<std::endl;
      // insert dtu defaults --> insert_dtu_def","INSERT INTO dtu_defaults \n"
      //      "(register_add, set_id, register_val) VALUES (:1,:2,:3)"
      Statement *statement = _statements["insert_dtu_def"];


      // We need to know how many iterations we will do with oracle.
      // Is there a better way to count all the channels we will write?
      std::vector<int> reg_values;
      bool defaults_done = false;
      for (auto &twrs : electronics_json["towers"].items()) {
        if (defaults_done) break; 
        auto &twr = electronics_json["towers"][twrs.key()];
        for (auto &chs : twr["channels"].items()) {
          if (defaults_done) break;
          auto &ch = twr["channels"][chs.key()];
          int channel = std::stoul(chs.key());
          // We use first channel not empty to get the default values of DTU chips
          if (ch["dtu"].size() == 0) continue;
          auto &dtu = ch["dtu"];
          for (auto &val : dtu) {
                std::cout<<"dtu "<<val<<std::endl;
                reg_values.push_back(val);
          }
          defaults_done = true;
        }
      } 
      
      std::cout << "iterations " << reg_values.size() << std::endl;
      statement->setMaxIterations(reg_values.size());
      try {
        int counter = 0;
        for (auto val: reg_values) {
            statement->setInt(1, counter);
            statement->setInt(2, set_id);
            statement->setInt(3, val);
            std::cout<< counter << " "<<reg_values.size()<<std::endl;
            if (counter < reg_values.size()-1) statement->addIteration();
            counter++;
        }
        statement->executeUpdate();
        conn->terminateStatement(statement);
      } catch(SQLException ex) {
          std::cout<<"Exception thrown for addDtuDefaults"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
      }
      return set_id;
    }

    int bcpdb_oracle::addVFEConfig(std::string tag, int full_calib_set_id, int full_ped_set_id, int dtu_def_set_id, int catia_def_set_id, int lpgbt_def_set_id) {
      int vfe_config_id = getNextVal("vfe_config_map_seq");
      std::cout<<"Generated vfe_config_id for vfe_config_map insert: "<<vfe_config_id<<std::endl;


      // insert_vfe_map --> "INSERT INTO vfe_config_map (vfe_config_id, tag, dtu_def_set_id, catia_def_set_id, pedestal_set_id, adc_set_id) \n"
      // " VALUES (:1,:2,:3,:4,:5,:6)"
      try{
        Statement *statement = _statements["insert_vfe_map"];
        statement->setInt(1, vfe_config_id);
        statement->setString(2, tag);
        statement->setInt(3, lpgbt_def_set_id);
        statement->setInt(4, dtu_def_set_id);
        statement->setInt(5, catia_def_set_id);
        statement->setInt(6, full_calib_set_id);
        statement->setInt(7, full_ped_set_id);
        statement->executeUpdate();
      } catch (SQLException ex){
        std::cout<<"Exception thrown for insert_vfe_map"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
      }
      
      return vfe_config_id;
    }

    

    void bcpdb_oracle::getAdcs(int full_calib_set_id, json &full_config) {

      // SELECT a.set_id, a.device_key, a.all_regs FROM adcs a \n"
      //      "LEFT JOIN calibration_set_map b ON a.set_id=b.set_id and a.device_key=b.device_key WHERE (b.adc_set_id=:1); 
      Statement *statement = _statements["getAdcsByFullSetId"];
      statement->setInt(1, full_calib_set_id);
      ResultSet* rs =statement->executeQuery();

      while (rs->next()) {
        int set_id = rs->getInt(1);
        long int device_key = rs->getNumber(2);
        Blob blob = rs->getBlob(3);
        int tower = getTowerFromDeviceKey(device_key);
        int channel = getTowerChNumFromDeviceKey(device_key);
        std::string adc = "adc"+std::to_string(getDeviceTypeFromDeviceKey(device_key));
        std::cout<<"device key "<< std::to_string(device_key) << " " << adc << std::endl;
        auto &device = full_config["towers"][std::to_string(tower)]["channels"][std::to_string(channel)][adc];
        if(blob.isNull()) {
          std::cout <<"Null Blob"<<std::endl;
          continue;
        } else {
          blob.open(OCCI_LOB_READONLY);
          const unsigned int BUFSIZE=76;
          unsigned char* all_regs = new unsigned char[BUFSIZE]();
          unsigned int readAmt=BUFSIZE;
          unsigned int offset=1;
 
          //reading readAmt bytes from offset 1
          blob.read(readAmt,all_regs,BUFSIZE,offset);
          for (int i=0; i<BUFSIZE; i++) {
            device.push_back((unsigned int)all_regs[i]);
          }
          blob.close();
        }
        
      }
    }

    void bcpdb_oracle::getPedestals(int full_ped_set_id, json &full_config) {
      // SELECT a.set_id, a.ch_key, a.vref, a.dac_hg, a.dac_lg, a.dig_ped_corr_hg, a.dig_ped_corr_lg, a.gain_switch_th_corr \n"
      //      " FROM pedestals a LEFT JOIN pedestal_set_map b ON a.set_id=b.set_id and a.ch_key=b.ch_key WHERE (b.pedestal_set_id=:1)
      Statement *statement = _statements["getPedsByFullSetId"];
      statement->setInt(1, full_ped_set_id);
      ResultSet* rs =statement->executeQuery();

      int counter = 0;
      while (rs->next()) {
        int set_id 	            = rs->getInt(1);
        int ch_key 	            = rs->getInt(2);
        int vref 	            = rs->getInt(3);
        int dac_hg              = rs->getInt(4);
	    int dac_lg              = rs->getInt(5);
	    int dig_ped_corr_hg     = rs->getInt(6);
	    int dig_ped_corr_lg     = rs->getInt(7);
	    int gain_switch_th_corr = rs->getInt(8);
        counter ++;
          
        int tower = getTowerFromChKey(ch_key);
        int tower_ch = getTowerChNumFromChKey(ch_key);
        auto &chObj = full_config["towers"][std::to_string(tower)]["channels"][std::to_string(tower_ch)];
        int reg3_val = (dac_lg << 8) + (dac_hg << 2) + 3;   
        //chObj["catia"][0x3] = reg3_val;
        full_config["towers"][std::to_string(tower)]["channels"][std::to_string(tower_ch)]["catia"][0x3] = reg3_val;
        int reg6_val = 0x0F+(vref<<4);
        chObj["catia"][0x6] = reg6_val;

        chObj["dtu"][0x5] = dig_ped_corr_hg;
        chObj["dtu"][0x6] = dig_ped_corr_lg;
        
        chObj["dtu"][0x7] = gain_switch_th_corr & 0xff;
        chObj["dtu"][0x8] = (gain_switch_th_corr >> 8) & 0xff;
        std::cout<<tower<<" "<<tower_ch<< " " <<chObj["catia"][0x6]<<std::endl;
      }
      std::cout<<"Pedestals counter: "<<counter<<std::endl;
    }

    void bcpdb_oracle::getCatiaDefaults(int catia_def_set_id, json &full_config) {
      // getCatiaDefBySetId --> "SELECT set_id, register_add, register_val FROM catia_defaults WHERE (set_id=:1)"
      Statement *statement = _statements["getCatiaDefBySetId"];
      statement->setInt(1, catia_def_set_id);
      ResultSet* rs =statement->executeQuery();
      int counter = 0;
      std::vector<int> catia_defs;
      while (rs->next()) {
        int set_id      = rs->getInt(1);
        int register_add = rs->getInt(2);
        int register_val = rs->getInt(3);
        catia_defs.push_back(register_val);
        counter ++;
      }

      
      try {
        if (!full_config.contains("towers")) throw std::runtime_error("\"Tower\" key missing in full_config json");
        for (auto &twrs : full_config["towers"].items()) {
          auto &twr = full_config["towers"][twrs.key()];
          for (auto &chs : twr["channels"].items()) {
            auto &ch = twr["channels"][chs.key()];
            for (auto val : catia_defs) {
              ch["catia"].push_back(val);
            }
          }
        }
      } catch (...) {
        std::cout<<"Error during writing CATIA defaults on json. Have you launched getAdcs first?"<<counter<<std::endl;
      } 
      std::cout<<"Catia default counter: "<<counter<<std::endl;
    };

    void bcpdb_oracle::getDtuDefaults(int dtu_def_set_id, json &full_config) {
      // getDtuDefBySetId --> "SELECT set_id, register_add, register_val FROM dtu_defaults WHERE (set_id=:1)"
      Statement *statement = _statements["getDtuDefBySetId"];
      statement->setInt(1, dtu_def_set_id);
      ResultSet* rs =statement->executeQuery();
      int counter = 0;
      std::vector<int> dtu_defs;
      while (rs->next()) {
        int set_id       = rs->getInt(1);
        int register_add = rs->getInt(2);
        int register_val = rs->getInt(3);
        dtu_defs.push_back(register_val);
        counter ++;
      }
      try { 
        if (!full_config.contains("towers")) throw std::runtime_error("\"Tower\" key missing in full_config json");
        for (auto &twrs : full_config["towers"].items()) {
          auto &twr = full_config["towers"][twrs.key()];
          for (auto &chs : twr["channels"].items()) {
            auto &ch = twr["channels"][chs.key()];
            for (auto val : dtu_defs) {
              ch["dtu"].push_back(val);
            }
          }
        }
      } catch (...) {
        std::cout<<"Error during writing DTU defaults on json. Have you launched getAdcs first?"<<std::endl;
      }
      std::cout<<"Dtu default counter: "<<counter<<std::endl;
    }; 
 
    void bcpdb_oracle::getLpgbtDefaults(int lpgbt_def_set_id, json &full_config) {
      // getLpgbtDefBySetId --> SELECT lpgbt_def_set_id, device_add, register_add, register_val FROM lpgbt_defaults WHERE (lpgbt_def_set_id=:1) ORDER BY device_add, register_add
      Statement *statement = _statements["getLpgbtDefBySetId"];
      statement->setInt(1, lpgbt_def_set_id);
      ResultSet* rs =statement->executeQuery();
      int counter = 0;
      std::map<unsigned int, std::map<unsigned int,unsigned int> > lpgbt_defs;
      while (rs->next()) {
        int set_id                 = rs->getInt(1);
        unsigned int device_add    = rs->getInt(2);
        unsigned int register_add  = rs->getInt(3);
        unsigned int register_val  = rs->getInt(4);
        lpgbt_defs[device_add][register_add]=register_val;
        counter ++;
      }
      try {
        if (!full_config.contains("towers")) throw std::runtime_error("\"Tower\" key missing in full_config json");
        for (auto &twrs : full_config["towers"].items()) {
          auto &twr = full_config["towers"][twrs.key()];
          for (auto& lpgbt : lpgbt_defs) {
            for (auto& reg : lpgbt.second) {
              twr["lpgbts"][std::to_string(lpgbt.first)][std::to_string(reg.first)] = reg.second;
            }
          }
        }
      } catch (...) {
        std::cout<<"Error during writing LpGBT defaults on json. Have you launched getAdcs first?"<<std::endl;
      }
      std::cout<<"Lpgbt default counter: "<<counter<<std::endl;

    }

    json bcpdb_oracle::getVFEConfig(int vfe_config_id) {

      //if (logger_ != nullptr) LOG4CPLUS_INFO(*logger_, "In getVFEConfig");

      json vfe_config;

      // getFullVFEConfigById --> SELECT vfe_config_id, tag, lpgbt_def_set_id, dtu_def_set_id, catia_def_set_id, pedestal_set_id, adc_set_id \n"
      //      " FROM vfe_config_map WHERE vfe_config_id=:1
      Statement *statement = _statements["getFullVFEConfigById"];
      statement->setInt(1, vfe_config_id);
      ResultSet* rs =statement->executeQuery();
      while (rs->next()) {
        vfe_config["vfe_config_id"] = rs->getInt(1);
        vfe_config["tag"] = rs->getString(2);
        vfe_config["lpgbt_def_set_id"] = rs->getInt(3);
        vfe_config["dtu_def_set_id"] = rs->getInt(4);
        vfe_config["catia_def_set_id"] = rs->getInt(5);
        vfe_config["pedestal_set_id"] = rs->getInt(6);
        vfe_config["adc_set_id"] = rs->getInt(7);
      }

      return vfe_config;
    }


    void bcpdb_oracle::addBoardType(int id, std::string &name, std::string &desc) {

      Statement *statement = _statements["insert_board_type"]; 
      statement->setInt(1, id);      
      statement->setString(2, name);
      statement->setString(3, desc);
      statement->executeUpdate();
    }

    void bcpdb_oracle::addBoardTypes(std::map<int, std::pair<std::string, std::string>> &board_types) {
      Statement *statement = _statements["insert_board_type"];
      try {
            statement->setMaxIterations(board_types.size());
            statement->setMaxParamSize(2,255);
            statement->setMaxParamSize(3,255);
            for (auto bt = board_types.begin(); bt != board_types.end(); ++bt) {
                std::string name;
                std::string description;
                int btid = bt->first;
                std::tie(name, description) = bt->second;
                statement->setInt(1, btid);
                statement->setString(2, name);
                statement->setString(3, description);
                if (bt != std::prev(board_types.end())) {
                    statement->addIteration();
                }
            }
            statement->executeUpdate();
            conn->terminateStatement(statement);
      } catch(SQLException ex) {
          std::cout<<"Exception thrown for addBoardTypes"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
      }
    }

    void bcpdb_oracle::addDeviceTypes(std::map<int, std::pair<std::string, std::string>> &device_types) {
      // INSERT INTO device_types ( device_type, name, description ) VALUES ( :1, :2, :3 );
      Statement *statement = _statements["insert_device_type"];
      try {
        statement->setMaxIterations(device_types.size());
        statement->setMaxParamSize(2,255);
        statement->setMaxParamSize(3,255);
   
        for (auto dt = device_types.begin(); dt != device_types.end(); ++dt) {
          bcp::device type = bcp::lookup_device_type(dt->first);
          if (type == device::Unknown) continue;
          std::string name;
          std::string desc;
          std::tie(name, desc) = dt->second;
          statement->setInt(1, type);
          statement->setString(2, name);
          statement->setString(3, desc);
          if (dt != std::prev(device_types.end())) {
             statement->addIteration();
          }
          //bcp::Device_Type dev_type;
          //dev_type.name = name;
          //dev_type.description = desc;
          //dev_type.type = type;
          //db.addDeviceType(dev_type);
        } 
        statement->executeUpdate();
        conn->terminateStatement(statement);
      } catch(SQLException ex) {
        std::cout<<"Exception thrown for addDeviceTypes"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
      }
    }

    void bcpdb_oracle::addSuperModule (unsigned int id) {
     /* Statement *statement = _statements["insert_supermodule"];
      try{
        statement->setInt(1, id);
        statement->executeUpdate();
        conn->terminateStatement(statement);
      } catch (SQLException ex){
        std::cout<<"Exception thrown for addDeviceTypes"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
      }*/
      std::cout<<"Not inserting anything, SM should be inserted by constdb script"<<std::endl;
    }

    int bcpdb_oracle::addTowers() {
        try{
            Statement *statement = _statements["insert_tower"];
            statement->setMaxIterations(37*70);
            for(int sm =0; sm<37; sm++){
                for(int tower=0; tower<70; tower++){
                    int tower_key = sm*100+tower;
                    statement->setInt(1, tower_key);
                    statement->setInt(2, tower);
                    statement->setInt(3, sm);
                    if(tower_key != 3669) statement->addIteration();
                }
            }
            statement->executeUpdate();
            conn->terminateStatement(statement);
        } catch (SQLException ex){
            std::cout<<"Exception thrown for addTowers"<< std::endl;
            std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
            std::cout<<ex.getMessage() << std::endl;
            return 1;
        }
        return 0; 
    }


    int bcpdb_oracle::addBoards() {
        try{
            Statement *statement = _statements["insert_board"];
            statement->setMaxIterations(37*70*8);
            std::vector <int> boards;
            for(int sm =0; sm<37; sm++){
                for(int tower=0; tower<70; tower++){
                    int tower_key = sm*100+tower;
                    if (tower < 68) boards = {0, 1, 2, 3, 4, 5, 6, 7};
                    else  boards = {0, 1, 5};
                    for(auto& board: boards){
                            int board_key = tower_key*100 + board;
                            statement->setInt(1, board_key);
                            statement->setInt(2, board);
                            statement->setInt(3, tower_key);
                            statement->setInt(4, 0);
                            if(board_key != 366905) statement->addIteration();
                    }
                }
            }
            statement->executeUpdate();
            conn->terminateStatement(statement);
        } catch (SQLException ex){
            std::cout<<"Exception thrown for addBoards"<< std::endl;
            std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
            std::cout<<ex.getMessage() << std::endl;
            return 1;
        }
        return 0; 
    }
   

   
    int bcpdb_oracle::addChannels() {
        try{
            Statement *statement = _statements["insert_channel"];
            statement->setMaxIterations(37*70*8*6);
            std::vector <int> boards;
            int max_channels;
            for(int sm =0; sm<37; sm++){
                for(int tower=0; tower<70; tower++){
                    int tower_key = sm*100+tower;
                    if (tower < 68) boards = {0, 1, 2, 3, 4, 5, 6, 7};
                    else  boards = {0, 1, 5};
                    for(auto& board: boards){
                            int board_key = tower_key*100 + board;
                            if ((board < 5) && (tower <68)) max_channels=5;
                            else if (board < 5 ) max_channels=6;
                            else if (board == 5 ) max_channels = 1;
                            else max_channels = 0; 
                            for (int channel=0; channel < max_channels; channel++){
                                int channel_key = board_key*100 + channel;
                                statement->setInt(1, channel_key);
                                statement->setInt(2, channel);
                                statement->setInt(3, board_key);
                                if(channel_key != 36690500) statement->addIteration();
                            }
                    }
                }
            }
            statement->executeUpdate();
            conn->terminateStatement(statement);
        } catch (SQLException ex){
            std::cout<<"Exception thrown for addChannels"<< std::endl;
            std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
            std::cout<<ex.getMessage() << std::endl;
            return 1;
        }
        return 0; 
    }



    int bcpdb_oracle::addDevices() {
        try{
            Statement *statement = _statements["insert_device"];
            statement->setMaxIterations(36*70*8*6*6);
            std::vector <int> boards;
            std::vector <int> devices;
            int max_channels;
            for(int sm =0; sm<37; sm++){
            //for(int sm =0; sm<1; sm++){
                for(int tower=0; tower<70; tower++){
                    int tower_key = sm*100+tower;
                    if (tower < 68) boards = {0, 1, 2, 3, 4, 5, 6, 7};
                    else  boards = {0, 1, 5};
                    for(auto& board: boards){
                            int board_key = tower_key*100 + board;
                            if ((board < 5) && (tower <68)) max_channels=5;
                            else if (board < 5 ) max_channels=6;
                            else if (board == 5 ) max_channels = 1;
                            else max_channels = 0; 
                            for (int channel=0; channel < max_channels; channel++){
                                unsigned int channel_key = board_key*100 + channel;
                                if (max_channels == 5) devices = {0,1,2,3};
                                else if (max_channels == 1) devices = {4,5,6,7,8,9};
                                else if ((max_channels == 6) && (channel == 0)) devices = {0,1,2,3};
                                else devices = {0,1,2}; 
                                for(auto &device: devices){
                                    long int device_key = channel_key*100 + device;
                                    Number deviceKeyNumber(device_key);
                                    //const Number test = Number(device_key);
                                    statement->setNumber(1, deviceKeyNumber);
                                    //statement->setInt(1, device_key);
                                    statement->setInt(2, device);
                                    statement->setInt(3, channel_key);
                                    if(device_key != 3669050009) statement->addIteration();
                                    //if(device_key != 69050009) statement->addIteration();
                                }
                            }
                    }
                }
            }
            statement->executeUpdate();
            conn->terminateStatement(statement);
        } catch (SQLException ex){
            std::cout<<"Exception thrown for addDevices"<< std::endl;
            std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
            std::cout<<ex.getMessage() << std::endl;
            return 1;
        }
        return 0; 
    }


    void bcpdb_oracle::addRegisterType(Register_Type &r) {
      try{
        Statement *statement = _statements["insert_register_type"];
        //statement->setMaxIterations(1);
        //statement->setMaxParamSize(5,255);
        //statement->setMaxParamSize(6,255);
        statement->setInt(1, r.device_type);
        statement->setInt(2, r.address);
        statement->setInt(3, r.width);
        statement->setInt(4, r.initial);
        statement->setString(5, r.name.c_str());
        statement->setString(6, r.description.c_str());
        statement->executeUpdate();
        //conn->terminateStatement(statement);
      } catch (SQLException ex){
        std::cout<<"Exception thrown for insert_register_type"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
      }
    }


    int bcpdb_oracle::getNextVal(std::string sequence_name) {
      Statement *statement = conn->createStatement("SELECT "+sequence_name+".NEXTVAL FROM DUAL");
      ResultSet* rs =statement->executeQuery();
      int nextVal = -1;
      while (rs->next()) {
        nextVal = rs->getInt(1);
      }
      return nextVal;
    }

    int bcpdb_oracle::getLatestVFEConfigId() {
      Statement *statement = conn->createStatement("select VFE_CONFIG_ID from (select VFE_CONFIG_ID from vfe_config_map order by VFE_CONFIG_ID desc) where rownum = 1");
      ResultSet* rs =statement->executeQuery();
      int lastVal = -1;
      while (rs->next()) {
        lastVal = rs->getInt(1);
      }
      return lastVal;
    }
   
    int bcpdb_oracle::getLatestLpgbtDefSetId() {
      Statement *statement = conn->createStatement("select LPGBT_DEF_SET_ID from (select LPGBT_DEF_SET_ID from lpgbt_defaults order by LPGBT_DEF_SET_ID desc) where rownum = 1");
      ResultSet* rs =statement->executeQuery();
      int lastVal = -1;
      while (rs->next()) {
        lastVal = rs->getInt(1);
      }
      return lastVal;
    } 

    int bcpdb_oracle::addRegisterSetMap(int set_id, std::vector<int> reg_id_vector) {
        try {
          Statement *statement = _statements["insert_set_map_entry"];
          statement->setMaxIterations(reg_id_vector.size());
          int counter = 0;
          for(auto& reg_id: reg_id_vector) {
            statement->setInt(1, set_id);
            statement->setInt(2, reg_id);
            //counter ++;
            if(counter != reg_id_vector.size()-1) statement->addIteration();
            counter ++;
          }
          statement->executeUpdate();
        } catch (SQLException ex) {
          std::cout<<"Exception thrown for addRegisterSetMap"<< std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
          return 1;
        }
        return 0;
    }

    int bcpdb_oracle::addRegisterSet() {
      int register_set_id = -1;
      try {
        Statement *statement = _statements["insert_register_set"];
        std::string note = "my_note";
        statement->setString(1, note.c_str());
        statement->registerOutParam(2, OCCIINT, sizeof(register_set_id));
        statement->executeUpdate();
        register_set_id = statement->getInt(2);
        //conn->terminateStatement(statement);
      } catch (SQLException ex) {
        std::cout<<"Exception thrown for addRegisterSet"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
        return -1;
      }
      return register_set_id; 
    }

    int bcpdb_oracle::addRegister(Register &r) {
 
      int reg_id;
      try{
        Statement *statement = _statements["insert_register"];

        statement->setInt(1, r.type.register_type_id);
        statement->setInt(2, r.device_id);
        statement->setInt(3, r.rvalue);
        statement->registerOutParam(4, OCCIINT, sizeof(reg_id));
        statement->executeUpdate();
        reg_id=statement->getInt(4);

      } catch (SQLException ex){
        std::cout<<"Exception thrown for addRegister"<< std::endl;
        std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
        std::cout<<ex.getMessage() << std::endl;
        return -1;
      }
      return reg_id; 
    }

    std::vector<int> bcpdb_oracle::getBoardChannels(int board_type, int tower_id, int supermodule=0) {
      std::vector<int> channels;
      Statement *statement = _statements["getBoardChannels"];
      statement->setInt(1, supermodule);
      statement->setInt(2, tower_id);
      statement->setInt(3, board_type);
      ResultSet* rs =statement->executeQuery();

      while (rs->next()) {
	    channels.push_back(rs->getInt(1)); 
      }
      return channels; 
    
    }

  std::vector<Register_Type> bcpdb_oracle::get_register_types(){
    std::vector<Register_Type> types;
    Statement *statement = _statements["register_types"];
    ResultSet* rs =statement->executeQuery();
    while (rs->next()){
        Register_Type r;
	    r.register_type_id = rs->getInt(1);
	    r.device_type = static_cast<device>(rs->getInt(2));
	    r.address = rs->getInt(3);
	    r.width =  rs->getInt(4);
	    r.initial = rs->getInt(5);
	    //r.name = reinterpret_cast<const char *>(rs->getString(5));
        r.name = rs->getString(6);
	    //r.description = reinterpret_cast<const char *>(rs->getString(6));
        r.description = rs->getString(7);
	    types.push_back(r); 
    }

    return types;

  }

  std::vector<Device> bcpdb_oracle::devices(int channel_id) {
      std::vector<Device> devices;
      Statement *statement = _statements["channel_devices"];

      statement->setInt(1, channel_id);
      ResultSet* rs =statement->executeQuery();
      while (rs->next()) {
        Device d;
        d.device_id = rs->getInt(1);
        d.dtype.type = static_cast<bcp::device>(rs->getInt(2));
        d.ch_id = channel_id;
        devices.push_back(d);
      }
      return devices;
  }

  int bcpdb_oracle::getDeviceFromTypeAndKey(int ch_key, device device_type) {
    
    Statement *statement = _statements["getDeviceFromTypeAndKey"];
    statement->setInt(1, ch_key);
    statement->setInt(2, device_type);
    ResultSet* rs =statement->executeQuery();

    int device_key = -1; 
    while (rs->next()) {
	    device_key =rs->getInt(1); 
    }
    return device_key; 
  }

  std::vector<Register> bcpdb_oracle::registers(int device_id) {
      std::vector<Register> registers;
      Statement *statement = _statements["device_registers"];
      statement->setInt(1, device_id);
      ResultSet* rs =statement->executeQuery();
      
      while (rs->next()) {
        Register r;
        r.rvalue = rs->getInt(1);
        r.device_id = device_id;
        r.type.name = rs->getString(2);
        r.type.address = rs->getInt(3);
        registers.push_back(r);
      }
      return registers;
  }


    bool bcpdb_oracle::memoryMode() {
      if (!prepare_statements()) {
        throw std::runtime_error("Preparing db statements went wrong....");
        exit(-1);
      }
      return  true;
    }

// ----------------------------
    bool bcpdb_oracle::prepare_statements() {

      try {
        std::cout << "Preparing statements" << std::endl;
    
        for (auto &pair: insertstrs) {
          prepare_statement(pair.first, pair.second);
        }
        for (auto &pair: selectstrs) {
          prepare_statement(pair.first, pair.second);
        }
        for (auto &pair: droptablestrs) 
          prepare_statement(pair.first, pair.second);
        }
      catch (std::runtime_error &err) {
        std::cout << "Problem creating statements" << std::endl;
        std::cout << err.what() << std::endl;
        return false;
      }
      return true;
    }

// ----------------------------
    void bcpdb_oracle::prepare_statement(const std::string &key, const std::string &cmd) {
      try {
        Statement *stmt = conn->createStatement (cmd);
        _statements[key] = stmt;
      } catch(SQLException ex) {
          std::cout<<"Exception thrown for prepare_statement"<<std::endl;
          std::cout<<"Error number: "<<  ex.getErrorCode() << std::endl;
          std::cout<<ex.getMessage() << std::endl;
        }

    }
    bool bcpdb_oracle::close() {
      env->terminateConnection(conn);
      return 0;
    }


}    
}
