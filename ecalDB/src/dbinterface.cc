#include "dbinterface.h"



namespace bcp {

  bcpdb::bcpdb() {
    _type = dbType::Base;
  }


  bcpdb::~bcpdb() {


  }

  bcpdb::bcpdb(const std::string uri, bool create) {


  }


  bool bcpdb::open(const std::string &uri, bool create) {
    return true;
  }
  
  bool bcpdb::close() {
    return true;
  }

  std::vector<std::string> bcpdb::tables() {
    std::vector<std::string> tables; 
    return tables; 

  }

  void bcpdb::create_table(const std::string &ctstmt) {


  }

  bool bcpdb::create_tables() {
    return true; 

  }

  bool bcpdb::fill_tables()  {
    return true;
  }
    

  bool bcpdb::check_key(const std::string &key) {
    return true;
  }

  void bcpdb::addBoard(int bkey, int btype, int tkey, int bcode) {

  }
  void bcpdb::addBoardType(int id, std::string &name, std::string &desc) {

  } 
  void bcpdb::addBoardTypes(std::map<int, std::pair<std::string, std::string>> &board_types) {

  }
  void bcpdb::addRegisterType(Register_Type &r) {

  }
  int bcpdb::addRegister(Register &r) {
    return 0; 
  }
 
  void bcpdb::addRegisterFast(std::vector<Register> &vr) {
    
  }
  int bcpdb::addRegisterSet() {
    return 0; 
  }
  void bcpdb::addSetMapEntry(int register_set_id, int register_id) {
    
  }

  int bcpdb::addDevice(const Device &d) {
    return 0; 
  }
  int bcpdb::addCATIA(const Catia &d) {
    return 0; 
  }
  void bcpdb::addDeviceType(Device_Type &dt) {
    return; 
  }
  void bcpdb::addChannel(int ch_key, int ch_id, int board_key) {
    return; 

  }

    
  void bcpdb::addTower(unsigned int tower, unsigned int sm) {


  }
  
  void bcpdb::addSuperModule(unsigned int id) {

  }
  void bcpdb::save(const std::string &fname) {

  }
  
  int bcpdb::getDeviceFromTypeAndKey(int ch_key, device device_type) {

    return 0; 
  }


  std::vector<int> bcpdb::getBoardChannels(int board_type, int tower_id, int supermodule) {
    std::vector<int> board_channels;
    return board_channels; 

  }
  
  std::vector<int> bcpdb::getSupermodules() {
    std::vector<int> smodules;

    return smodules; 

  }
  
  std::vector<keyid_pair> bcpdb::getTowers() {
    std::vector<keyid_pair> towers;
    return towers; 

  }

  std::vector<Device> bcpdb::devices() {
    std::vector<Device> devs;
    return devs;
    
  }
  
  std::vector<Device> bcpdb::devices(int channel_id) {
    std::vector<Device> devs;
    return devs; 

  }
  std::vector<Device_Type> bcpdb::device_types() {
    std::vector<Device_Type> dtypes;
    return dtypes;

  }
  std::vector<Register> bcpdb::registers() {
    std::vector<Register> regs;
    return regs; 

  }
  std::vector<Register> bcpdb::registers(int device_id) {
    std::vector<Register> regs;
    return regs; 

  }
  std::vector<Register_Type> bcpdb::register_types() {
    std::vector<Register_Type> rtypes;
    return rtypes; 

  }
  std::vector<Register_Type> bcpdb::register_types(device type) {
    std::vector<Register_Type> rtypes;
    return rtypes;

  }

  std::vector<keyid_pair > bcpdb::channels() {
    std::vector<keyid_pair > chs;
    return chs; 

  }

  std::vector<keyid_pair> bcpdb::channels(int board_type, int tower_id, int supermodule) {

    std::vector<keyid_pair > chs;
    return chs; 
  }


}
