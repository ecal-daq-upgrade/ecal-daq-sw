#include "vfe.h"
#include <fstream> 
#include <sstream> 
#include <map>
#include <stdexcept>
#include <string> 


namespace bcp {


  device lookup_device_type(int type) {
    switch (type) {
    case 0:
      return ADCH;
    case 1:
      return ADCL;
    case 2:
      return DTU;
    case 3:
      return CATIA;
    case 4:
      return LPGBT0;
    case 5:
      return LPGBT1;
    case 6:
      return LPGBT2;
    case 7:
      return LPGBT3;
    case 8:
      return VTRX;
    case 9:
      return SCA;
    default:
      return Unknown;
  }
  }; 
  
  Register::Register() {};


  Register_Type::Register_Type() {}; 


  Device::Device() {
    device_id = 0;
    ch_id = 0;

  };


  void Device::print_registers() {

    for (const auto& pair : registers) {
      const std::string &rkey = pair.first;
      const Register &reg = pair.second; 

      std::cout << rkey << ":" << reg.type.name << "," << reg.type.address << "," << reg.type.description << std::endl; 

    }
  

  };


  Register Device::lookup(const std::string &rname) {

    try {
      return registers.at(rname); 
    }
    catch (std::out_of_range &err) {
      return Register();
    }

  }
  

  void Device::insert(const Register &reg) {};

  bool Device::load_from_csv(const std::string csvfile) {
    /*std::fstream fin(csvfile);

    if (!fin.is_open())
      return false;


    std::string line;
    std::map<std::string, std::vector<std::string> > rvalues;
    std::map<unsigned int, std::string> nmap; 

    std::getline(fin, line);
    std::istringstream input(line);
    // fill header entries
    unsigned int idx = 0; 
    for (std::string part; std::getline(input, part, ',');) {
      rvalues[part] = std::vector<std::string>();
      nmap[idx] = part; 
      idx++; 
    }

    // fill map with values
  
    while (std::getline(fin, line)) {
      try {
	std::istringstream input(line);
	std::vector<std::string> parts;
	unsigned int idx = 0; 
	for (std::string part; std::getline(input, part, ',');) {
	  std::string rname = nmap[idx];
	  rvalues[rname].push_back(part);
	  idx++; 
	}
      }
      catch (std::out_of_range &err) {
	std::cerr << "Extra entry on line encountered loading from csv" << std::endl; 
	continue; 
      }
    }

    std::vector<std::string> keys; 
    for (auto &key : rvalues) {
      std::cout << key.first << "\t";
      keys.push_back(key.first); 
    }
    std::cout << std::endl; 

    int keycount = rvalues[keys[0]].size(); 
    for (int i = 0; i < keycount; i++) {
      for (int k = 0; k < keys.size(); k++) {
	std::cout << rvalues[keys[k]][i] << "\t"; 
      }
      std::cout << std::endl; 
    }

    for(int k = 0; k < keycount; k++) {
      Register r;
      r.name = rvalues["Name"][k];
      r.address = std::stoul(rvalues["Address"][k], nullptr, 16);
      r.size = std::stoul(rvalues["Width"][k], nullptr, 16);
      r.default_value = std::stoul(rvalues["Initial"][k], nullptr, 16);
      r.description =  rvalues["Description"][k];

      registers[r.name] = r; 
    }
    */
    return true;

  }


  Device_Type::Device_Type() {
    name = "Unknown";
    description = "";
    type = Unknown; 
  }


  
}
