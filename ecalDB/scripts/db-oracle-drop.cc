#include "dbinterface.h"
#include "dboracle.h"

#include "vfe.h"
#include "cxxopts.hpp"

//#include <any>
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <vector>
#include <map>
#include <fstream>
#include <unistd.h>



using namespace bcp; 
using namespace bcp::myoracle;
using namespace oracle::occi;


int main(int argc, char **argv) {

  cxxopts::Options options("Insert Tool", "A program for inserting various bcp related data into a SQLite DB.");
  options.add_options()
    ("h,help", "Print Program Use")
    ("m,bm", "Comma-separated Register bitmaps file", cxxopts::value<std::string>());

    // allow unrecognized opts
  options.allow_unrecognised_options();

  cxxopts::ParseResult result;
  try { 
    result = options.parse(argc, argv);
    //print_options(result); 
    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0); 
    }
  }
  catch (cxxopts::exceptions::missing_argument e) {
    std::cout << e.what() << std::endl;
    exit(-1); 
  }

  //try and open db
  const std::string userName = "ecal_p2ug_conf";
  const std::string password = "Ecal_conf_2024";
  const std::string connectString = "(DESCRIPTION = \
        (ADDRESS=(PROTOCOL = TCP)(HOST = int2r-s.cern.ch)(PORT = 10121)) \
        (CONNECT_DATA= (SERVICE_NAME = int2r_lb.cern.ch) \
        (SERVER = DEDICATED)) \
        )";
  
  bcpdb_oracle db(userName, password, connectString, false);

  db.memoryMode();

  db.dropAllTables();  
 
  db.close(); 
      
  return 0;
      
}     
      
         
    
   
