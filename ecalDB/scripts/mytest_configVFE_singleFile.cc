// Compile and launch

// g++ -std=gnu++11  -o mytest mytest.cpp -I/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -L/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -lwiscrpcsvc
// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev
// ./mytest


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiscrpcsvc.h>
#include <fstream>
#include <wiscrpcsvc.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace wisc;

#define MEMORY_TEST

int main(int argc, char *argv[]) {

    RPCSvc rpc;
    try {
        rpc.connect(argv[1]);
    }
    catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
    }
    catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
    }

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)

    RPCMsg req, rsp;

#ifdef MEMORY_TEST
    try {
        ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
    }
    STANDARD_CATCH;
    
    req = RPCMsg("fwmodule.configurevfesfromsinglefile");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.configurevfesfromsinglefile" <<std::endl;

        std::ifstream* f1 = new std::ifstream("./all_registers.json");
        json fullconfig = json::parse(*f1);
        for (json::iterator it = fullconfig["towers"].begin(); it != fullconfig["towers"].end(); ++it) {
            std::string towernb = it.key();
            req.set_word("towernb", std::atoi(towernb.c_str()));
            json data_single_tower = fullconfig["towers"][towernb]; 
            std::string jsonstring = fullconfig.dump();
            req.set_string("configjsonstring", jsonstring);
            rsp = rpc.call_method(req);
        }
    }
    STANDARD_CATCH;


#endif


    return 0;
}
