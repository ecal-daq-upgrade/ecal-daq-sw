#include <nlohmann/json.hpp>
#include "dbinterface.h"
#include "dboracle.h"

#include "vfe.h"
#include "cxxopts.hpp"

//#include <any>
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <vector>
#include <map>
#include <fstream>
#include <unistd.h>


using json = nlohmann::json;
using namespace bcp; 
using namespace bcp::myoracle;
using namespace oracle::occi;


std::map<unsigned int, unsigned int> read_lpgbt_conf_file(std::string filepath) {
  std::fstream filein(filepath);
  if (!filein.is_open()) {
    std::cout << "Could not open config file " << filepath << std::endl;
    throw std::invalid_argument("File not found");
  }
  std::cout << "Parsing file lines..." << std::endl;
  std::string line;
  std::map<unsigned int, unsigned int> reg_map;
  while (std::getline(filein, line)) {
    if (line.rfind("#", 0) == 0) continue; // it is a comment, we can skip it
    std::string reg_add = line.substr(0, line.find(" "));
    std::string reg_val = line.substr(line.find(" ")+1);
    std::cout<<reg_add<<" "<<reg_val<<std::endl;

    reg_map[stoul(reg_add,nullptr,16)] = stoul(reg_val,nullptr,16);
  }
  return reg_map;
}


int main(int argc, char **argv) {

  if (argc !=5 ) {
      std::cout<<"Wrong number of arguments. It requires a path to the config file of each of the 4 lpgbts"<<std::endl;
      return 1;
  }
  cxxopts::Options options("Fill lpgbt defaults table", "A script to fill FE table");
  options.add_options()
    ("h,help", "Print Program Use");

    // allow unrecognized opts
  options.allow_unrecognised_options();

  cxxopts::ParseResult result;
  try { 
    result = options.parse(argc, argv);
    //print_options(result); 
    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0); 
    }
  }
  catch (cxxopts::exceptions::missing_argument e) {
    std::cout << e.what() << std::endl;
    exit(-1); 
  }

  //try and open db
  const std::string userName = "ecal_p2ug_conf";
  const std::string password = "Ecal_conf_2024";
  const std::string connectString = "(DESCRIPTION = \
        (ADDRESS=(PROTOCOL = TCP)(HOST = int2r-s.cern.ch)(PORT = 10121)) \
        (CONNECT_DATA= (SERVICE_NAME = int2r_lb.cern.ch) \
        (SERVER = DEDICATED)) \
        )";
  
  bcpdb_oracle db(userName, password, connectString, false);

  db.memoryMode();

  std::string lpgbtpath_master = argv[1];
  std::string lpgbtpath_slave1 = argv[2];
  std::string lpgbtpath_slave2 = argv[3];
  std::string lpgbtpath_slave3 = argv[4];
  
  std::map<unsigned int, std::map<unsigned int,unsigned int> > full_reg_map;
  std::map<unsigned int,unsigned int> reg_map;
  
  reg_map = read_lpgbt_conf_file(lpgbtpath_master);  
  full_reg_map[113] = reg_map;
  reg_map = read_lpgbt_conf_file(lpgbtpath_slave1);
  full_reg_map[114] = reg_map;
  reg_map = read_lpgbt_conf_file(lpgbtpath_slave2);
  full_reg_map[115] = reg_map;
  reg_map = read_lpgbt_conf_file(lpgbtpath_slave3);
  full_reg_map[116] = reg_map;

  for (auto const& lpgbt : full_reg_map) {
    for (auto const& reg : lpgbt.second) {
      unsigned int reg_add = reg.first;
      unsigned int reg_val = reg.second;
      unsigned int lpgbt_add = lpgbt.first;
    }
  }
  int lpgbt_def_set_id = db.addLpgbtDefaults(full_reg_map);
  std::cout << "New LpGBT defaults set id: "<<lpgbt_def_set_id<< std::endl;

  db.close(); 
      
  return 0;
      
}     
      
         
    
   
