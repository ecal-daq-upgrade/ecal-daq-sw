#include <nlohmann/json.hpp>
#include "dbinterface.h"
#include "dboracle.h"

#include "vfe.h"
#include "cxxopts.hpp"

//#include <any>
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <vector>
#include <map>
#include <fstream>
#include <unistd.h>


using json = nlohmann::json;
using namespace bcp; 
using namespace bcp::myoracle;
using namespace oracle::occi;


int main(int argc, char **argv) {

  if (argc !=2 ) {
      std::cout<<"Wrong number of arguments. Json file path required"<<std::endl;
      return 1;
  }
  cxxopts::Options options("Create new VFE tables", "A script to generate new VFE tables in the oracle DB");
  options.add_options()
    ("h,help", "Print Program Use");

    // allow unrecognized opts
  options.allow_unrecognised_options();

  cxxopts::ParseResult result;
  try { 
    result = options.parse(argc, argv);
    //print_options(result); 
    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0); 
    }
  }
  catch (cxxopts::exceptions::missing_argument e) {
    std::cout << e.what() << std::endl;
    exit(-1); 
  }

  //try and open db
  const std::string userName = "ecal_p2ug_conf";
  const std::string password = "Ecal_conf_2024";
  const std::string connectString = "(DESCRIPTION = \
        (ADDRESS=(PROTOCOL = TCP)(HOST = int2r-s.cern.ch)(PORT = 10121)) \
        (CONNECT_DATA= (SERVICE_NAME = int2r_lb.cern.ch) \
        (SERVER = DEDICATED)) \
        )";
  
  bcpdb_oracle db(userName, password, connectString, false);

  db.memoryMode();

//  std::string jsonname = "jsons/all_registers.json";
  std::string jsonname = argv[1];
  
  std::fstream jin(jsonname);
  if (!jin.is_open()) {
    std::cout << "Could not open json file " << jsonname << std::endl;
    exit(-1);
  }
  std::cout << "Parsing json lines..." << std::endl;
  std::stringstream input;
  for (std::string line; std::getline(jin, line);) {
    input << line;
  }
  auto parsed_json =  json::parse(input.str());
  std::cout<<"after parse"<<std::endl;  
  json cal_set_ids = db.addAdcs(parsed_json);
  std::cout<<"after adcs"<<std::endl; 
  int full_calib_set_id = db.addCalibrationSet(cal_set_ids);
  std::cout<<"after calibration"<<std::endl;
  json ped_set_ids = db.addPedestals(parsed_json);
  int full_ped_set_id = db.addPedestalSet(ped_set_ids);
  std::cout<<"after calibration"<<std::endl;
  int catia_def_set_id = db.addCatiaDefaults(parsed_json);
  int dtu_def_set_id = db.addDtuDefaults(parsed_json);
  
  std::string vfe_tag = "Just a test";
  int lpgbt_def_set_id = db.getLatestLpgbtDefSetId();
  std::cout << "Using lpgbt_def_set_id: " << lpgbt_def_set_id << std::endl;
  int vfe_config_id = db.addVFEConfig(vfe_tag, full_calib_set_id, full_ped_set_id, dtu_def_set_id, catia_def_set_id, lpgbt_def_set_id);
  std::cout<<"\nGenerated VFE config: "<<vfe_config_id<<"\n"<<std::endl; 


  db.close(); 
      
  return 0;
      
}     
      
         
    
   
