#include <nlohmann/json.hpp>
#include "dbinterface.h"
#include "dboracle.h"

#include "vfe.h"
#include "cxxopts.hpp"

//#include <any>
#include <iomanip>
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <vector>
#include <map>
#include <fstream>
#include <unistd.h>


using json = nlohmann::json;
using namespace bcp; 
using namespace bcp::myoracle;
using namespace oracle::occi;


int main(int argc, char **argv) {

  if (argc != 2) {
    std::cout<<"VFE config id missing. Launch the script like: ./db-oracle-get-vfe 1"<<std::endl;
    return 1;
  }


  cxxopts::Options options("Get VFE config", "Script to get a VFE configuration from the Oracle DB by vfe_config_id");
  options.add_options()
    ("h,help", "Print Program Use");

    // allow unrecognized opts
  options.allow_unrecognised_options();

  cxxopts::ParseResult result;
  try { 
    result = options.parse(argc, argv);
    //print_options(result); 
    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0); 
    }
  }
  catch (cxxopts::exceptions::missing_argument e) {
    std::cout << e.what() << std::endl;
    exit(-1); 
  }

  //try and open db
  const std::string userName = "ecal_p2ug_conf";
  const std::string password = "Ecal_conf_2024";
  const std::string connectString = "(DESCRIPTION = \
        (ADDRESS=(PROTOCOL = TCP)(HOST = int2r-s.cern.ch)(PORT = 10121)) \
        (CONNECT_DATA= (SERVICE_NAME = int2r_lb.cern.ch) \
        (SERVER = DEDICATED)) \
        )";
  
  bcpdb_oracle db(userName, password, connectString, false);

  db.memoryMode();
  int vfe_config_id = atoi(argv[1]);  
  json vfe_config = db.getVFEConfig(vfe_config_id);
  json full_config;
  // getAdcs before everything else!!!!!!! 
  db.getAdcs(vfe_config["adc_set_id"], full_config);
  // Then we need to prepare the defaults first
  db.getCatiaDefaults(vfe_config["catia_def_set_id"], full_config);
  db.getDtuDefaults(vfe_config["dtu_def_set_id"], full_config);
  // Only after the defaults we can do this action
  db.getPedestals(vfe_config["pedestal_set_id"], full_config);
  db.getLpgbtDefaults(vfe_config["lpgbt_def_set_id"], full_config);
  db.close(); 
 
  std::ofstream o("all_registers_from_db.json");
  o << std::setw(4) << full_config << std::endl;
     
  return 0;
      
}     
      
         
    
   
