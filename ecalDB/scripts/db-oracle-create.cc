#include "dbinterface.h"
#include "dboracle.h"

#include "vfe.h"
#include "cxxopts.hpp"

//#include <any>
#include <iostream> 
#include <stdexcept>
#include <string> 
#include <vector>
#include <map>
#include <fstream>
#include <unistd.h>



using namespace bcp; 
using namespace bcp::myoracle;
using namespace oracle::occi;


std::map<std::string, std::vector<std::string> > read_csv_parts(const std::string &fname) {

  std::fstream fin(fname);
  std::map<std::string, std::vector<std::string> > rvalues; 
  
  if (!fin.is_open())
    return rvalues;

  std::string line;

  std::map<unsigned int, std::string> nmap; 

  std::getline(fin, line);
  std::istringstream input(line);
  // fill header entries
  unsigned int idx = 0; 
  for (std::string part; std::getline(input, part, ',');) {
    //    std::cout << part << ","; 
    rvalues[part] = std::vector<std::string>();
    nmap[idx] = part; 
    idx++; 
  }

  // fill map with values
  
  while (std::getline(fin, line)) {
    try {
      std::istringstream input(line);
      std::vector<std::string> parts;
      unsigned int idx = 0; 
      for (std::string part; std::getline(input, part, ',');) {
  //    std::cout << nmap[idx] << "," << part << std::endl; 
	std::string rname = nmap[idx];
	rvalues[rname].push_back(part);
	idx++; 
      }
    }
    catch (std::out_of_range &err) {
      std::cerr << "Extra entry on line encountered loading from csv" << std::endl; 
      continue; 
    }
  }
  return rvalues; 


}



std::map<std::pair<int,int>, bcp::Register> load_registers(std::string fname) {

  std::map<std::pair<int,int>, bcp::Register> registers;

  auto rvalues = read_csv_parts(fname);

  std::vector<std::string> keys;
  for (auto &key : rvalues) {
    //std::cout << key.first << "\t";
    keys.push_back(key.first);
  }
  //  std::cout << std::endl;

  int keycount = rvalues[keys[0]].size();
  /*  for (int i = 0; i < keycount; i++) {
    for (int k = 0; k < keys.size(); k++) {
      std::cout << rvalues[keys[k]][i] << "\t";
    }
    std::cout << std::endl;
    }*/

  for(int k = 0; k < keycount; k++) {
    bcp::Register r;
    r.type.name = rvalues["Name"][k];
    r.type.address = std::stoul(rvalues["Address"][k], nullptr, 16);
    r.type.width = std::stoul(rvalues["Width"][k], nullptr, 10);
    r.type.initial = std::stoul(rvalues["Initial"][k], nullptr, 16);
    r.type.description =  rvalues["Description"][k];
    r.type.device_type = static_cast<bcp::device>(std::stoul(rvalues["Device"][k], nullptr, 16));

    r.type.access = std::stoul(rvalues["Access"][k]);
    registers[std::make_pair(r.type.device_type,r.type.address)] = r;
  }

  return registers;
}

bool check_file(std::string &fname) {


  std::fstream fin(fname);
  if (!fin.is_open())
    return false;
    
  return true;
}

void print_options(cxxopts::ParseResult &result) {

  std::cout << "List of options selected..." << std::endl; 
  for (auto &r : result) {
    if (result.count(r.key())) {
      std::cout << r.key() << std::endl;
    }

  }

}; 

int chCount(int btype) {
	switch(btype) {
	case 0:
	  return 5;
	case 1:
	  return 5;
	case 2:
	  return 5;
	case 3:
	  return 5;
	case 4:
	  return 5;
	case 5:
	  return 1;
	case 6:
	  return 1;
	default:
	  return 1;
	}
}


int main(int argc, char **argv) {

  cxxopts::Options options("Insert Tool", "A program for inserting various bcp related data into a SQLite DB.");
  options.add_options()
    ("h,help", "Print Program Use")
    ("f,file", "SQLite Database File To Create", cxxopts::value<std::string>()->default_value("test.db"))
    ("b,boardfile", "Board csv file for Board Types", cxxopts::value<std::string>())
    ("d,vfedevfile", "VFE Device csv file", cxxopts::value<std::string>())
    ("r,regfile", "CSV Device Register File", cxxopts::value<std::vector<std::string>>())
    ("m,bm", "Comma-separated Register bitmaps file", cxxopts::value<std::string>());


    // allow unrecognized opts
  options.allow_unrecognised_options();

  cxxopts::ParseResult result;
  try { 
    result = options.parse(argc, argv);
    //print_options(result); 
    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0); 
    }
  }
  catch (cxxopts::exceptions::missing_argument e) {
    std::cout << e.what() << std::endl;
    exit(-1); 
  }

  
  std::string dbname = result["file"].as<std::string>();
  if (check_file(dbname)) {
    std::cout << "DB already exists!" << std::endl;
    exit(-1);
  }

  //try and open db
      const std::string userName = "ecal_p2ug_conf";
      const std::string password = "Ecal_conf_2024";
      const std::string connectString = "(DESCRIPTION = \
        (ADDRESS=(PROTOCOL = TCP)(HOST = int2r-s.cern.ch)(PORT = 10121)) \
        (CONNECT_DATA= (SERVICE_NAME = int2r_lb.cern.ch) \
        (SERVER = DEDICATED)) \
        )";
  
  bcpdb_oracle db(userName, password, connectString, true);

  db.memoryMode();


  const int smcount = 36; 
  const int twrcount = 2;
  const int chcount = 25;

  // Board types
  std::map<int, std::pair<std::string, std::string>> board_types = {
    {0, {"vfe0", "vfe0"}},
    {1, {"vfe1", "vfe1"}},
    {2, {"vfe2", "vfe2"}},
    {3, {"vfe3", "vfe3"}},
    {4, {"vfe4", "vfe4"}},
    {5, {"FE", "FE"}},
    {6, {"LVR", "LVR"}},
    {7, {"MB", "MB"}}
  };

  //Tower Device Types
  std::map<int, std::pair<std::string, std::string> > device_types = {
    {0, {"ADCH", "LiteDTU High ADC Registers"}},
    {1, {"ADCL", "LiteDTU Low ADC Registers"}},
    {2, {"DTU", "FE Readout"}}, // LiTE-DTU
    {3, {"CATIA", "FE Amplifier"}},
    {4, {"LPGBT0", "LPGBT0"}},
    {5, {"LPGBT1", "LPGBT1"}},
    {6, {"LPGBT2", "LPGBT2"}},
    {7, {"LPGBT3", "LPGBT3"}},
    {8, {"VTRX", "VTRX"}},
    {9, {"SCA", "SCA"}}

  };
   
  db.addBoardTypes(board_types);
  db.addDeviceTypes(device_types);

  // try to insert the first full supermodule
  try {
    int sm = 0;
    //db.addSuperModule(sm); //SM created with Hardware script 
   // for (int twr = 0; twr < twrcount; twr++) {
      std::cout<<"#### adding towers"<<std::endl;
      db.addTowers(); //all towers for all SMs

      std::cout<<"### adding boards"<<std::endl;
      db.addBoards();

      std::cout<<"### adding channels"<<std::endl;
      db.addChannels();

      std::cout<<"### adding devices"<<std::endl;
      db.addDevices();

      std::cout<<"### adding register_types"<<std::endl;

      // Insert register types
      if (result.count("regfile")) {
        std::map<std::pair<int,int>, bcp::Register> regs;
        std::vector<std::string> rfiles = result["regfile"].as<std::vector<std::string>>();
        for (auto regfile : rfiles) {
          std::cout << "Loading Registers from " << regfile << std::endl;
          regs = load_registers(regfile);
          for (auto &reg : regs) {
              db.addRegisterType(reg.second.type);
          }
        }
      }

    // save
    //db.save(dbname); 
  }
  catch (std::runtime_error err) {
    std::cout << "Error during supermodule creation and filling:" << err.what() << std::endl;
    exit(-1);
  }
  db.close(); 
      
  return 0;
      
}     
      
         
    
   
