// Compile and launch

// g++ -std=gnu++11  -o mytest mytest.cpp -I/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -L/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -lwiscrpcsvc
// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev
// ./mytest


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiscrpcsvc.h>
using namespace wisc;

#define MEMORY_TEST

int main(int argc, char *argv[]) {

    RPCSvc rpc;
    try {
        rpc.connect(argv[1]);
    }
    catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
    }
    catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
    }

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)

    RPCMsg req, rsp;

#ifdef MEMORY_TEST
    try {
        ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
    }
    STANDARD_CATCH;
/*
    req = RPCMsg("fwmodule.configurefe");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.configurefe" <<std::endl;
        rsp = rpc.call_method(req);
    }
    STANDARD_CATCH;



    req = RPCMsg("fwmodule.fastresetvfe");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.fastresetvfe" <<std::endl;
        rsp = rpc.call_method(req);
    }
    STANDARD_CATCH;


*/
    req = RPCMsg("fwmodule.fastresetvfe");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.fastresetvfe" <<std::endl;
        rsp = rpc.call_method(req);
    }
    STANDARD_CATCH;


#endif


    return 0;
}
