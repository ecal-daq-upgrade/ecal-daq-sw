// Compile and launch

// g++ -std=gnu++11  -o mytest mytest.cpp -I/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -L/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -lwiscrpcsvc
// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev
// ./mytest

#include <typeinfo>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiscrpcsvc.h>
#include <iomanip>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace wisc;


#define MEMORY_TEST

int main(int argc, char *argv[]) {

    if (argc != 3) {
      std::cout<<"Wrong number of arguments"<<std::endl;
      std::cout<<"Provide elm IP as first and towers string as second argument"<<std::endl;
      return 1;
    }


    RPCSvc rpc;
    try {
        rpc.connect(argv[1]);
    }
    catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
    }
    catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
    }

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)

    RPCMsg req, rsp;

#ifdef MEMORY_TEST
    try {
        ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
    }
    STANDARD_CATCH;
    
    req = RPCMsg("fwmodule.getAllVFERegisters");
    rsp = RPCMsg();


    std::string towers = argv[2];

    try {
            
        std::cout<<"Calling fwmodule.getAllVFERegisters" <<std::endl;
        req.set_string("towers", towers); 
        rsp = rpc.call_method(req);
        std::cout<<"After call method"<<std::endl;
        std::string jsonstring = rsp.get_string("registers");
        std::cout<<jsonstring<< std::endl;
        json fullconfig = json::parse(jsonstring);
        

        // CATIA block, as long as we can not read from regs directly
        std::string filename = "/home/daqpro/bcp_controller_h4_oct2021/daq_values_bcp1.dat";
        std::ifstream* f1 = new std::ifstream(filename);
        if (f1->fail()) {
          std::cout<<"File "<<filename<<" could not be opened."<<std::endl;
          return 1;
        }

        json catia_json = json::parse(*f1);
        // loop over tower
        for (json::iterator it = fullconfig["towers"].begin(); it != fullconfig["towers"].end(); ++it) {
            std::string towernb = it.key();
            std::cout<<"tower "<< towernb << std::endl;
        
            // catia reg 0x0-0x2: not important
            // catia reg 0x3: dac val
            std::string catia_dac_g1 = "catia_dac_g1_t" + towernb;
            std::string catia_dac_g10 = "catia_dac_g10_t" + towernb;
            std::string vref = "vref_t" + towernb;
            for (int ch=0; ch<25; ch++) {
                auto &chObj = fullconfig["towers"][towernb]["channels"][std::to_string(ch)];
                if (chObj["catia"].size() == 0 &&
                    chObj["dtu"].size() == 0 &&
                    chObj["adc0"].size() == 0 &&
                    chObj["adc1"].size() == 0) {
                    
                    fullconfig["towers"][towernb]["channels"].erase(std::to_string(ch));
                    continue;
                }
                    
                //if (fullconfig["towers"][towernb]["channels"][std::to_string(ch)]["catia"].size() == 0) continue;
                int lg_dac = catia_json[catia_dac_g1][ch];
                int hg_dac = catia_json[catia_dac_g10][ch];
                int reg3_val = (lg_dac << 8) + (hg_dac << 2) + 3;
                fullconfig["towers"][towernb]["channels"][std::to_string(ch)]["catia"][0x3] = reg3_val;
            // catia reg 0x4: vref
                fullconfig["towers"][towernb]["channels"][std::to_string(ch)]["catia"][0x4] = 0;
            // catia reg 0x5: normal mode
                fullconfig["towers"][towernb]["channels"][std::to_string(ch)]["catia"][0x5] = 0xFFF;
            // catia reg 0x6: vref
                int vref_val = catia_json[vref][ch];
                int reg6_val = 0x0F+(vref_val<<4);
                fullconfig["towers"][towernb]["channels"][std::to_string(ch)]["catia"][0x6] = reg6_val;
            }
        }

        std::ofstream o("all_registers.json");
        o << std::setw(4) << fullconfig << std::endl;
    }
    STANDARD_CATCH;


#endif


    return 0;
}
