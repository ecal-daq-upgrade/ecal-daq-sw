// Compile and launch

// g++ -std=gnu++11  -o mytest mytest.cpp -I/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -L/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -lwiscrpcsvc
// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev
// ./mytest


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiscrpcsvc.h>
#include <fstream>
#include <wiscrpcsvc.h>
#include <nlohmann/json.hpp>
#include "db.h"
#include "vfe.h"

using json = nlohmann::json;
using namespace wisc;
using namespace bcp;

std::string lookup_device_type(vfe_device dtype) {

  std::string type ="";
  switch(dtype) {
  case CATIA:
    type = "catia";
    break;
  case LiteDTU:
    type = "dtu";
    break;
  case ADC_L:
    type = "adc0";
    break;
  case ADC_H:
    type = "adc1";
    break;
  default:
    type = "Unknown";
    break;
  };

  return type;
}



#define MEMORY_TEST

int main(int argc, char *argv[]) {

    RPCSvc rpc;
    try {
        rpc.connect(argv[1]);
    }
    catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
    }
    catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
    }

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)

    RPCMsg req, rsp;

#ifdef MEMORY_TEST
    try {
        ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
    }
    STANDARD_CATCH;
    
    std::cout<< "Contact DB and extract json" << std::endl;
    std::string dbname = "/mnt/ssda/nfs/elm_shared/bcpdb/test3.db";
    int supermodule = 0;
    int tower = 0;
    json output;
    
    bcpdb db(dbname, false);
    auto reg_types = db.register_types();
    std::map<std::pair<int,int>, Register_Type> regmap;
    for (const auto &type : reg_types) {
      vfe_device dtype = type.device_type;
      auto key = std::make_pair(dtype, type.address);
      regmap[key] = type;
    }
    int tower_id = supermodule*100+tower;
    std::string tstr = std::to_string(tower);
    output[tstr] = json();
    auto ch_ids = db.channels(tower_id, supermodule);
    for (auto ch : ch_ids) {
      std::string cstr = std::to_string(ch-tower_id);
      output[tstr][cstr] = json();
      std::cout<<output.dump()<<std::endl;
      auto devs = db.devices(ch);
      for (const auto &dev :devs) {
        // currently no guarantee of order by address, will fix....
        auto rvalues = db.registers(dev.device_id);
        std::vector<int> values;
        for (auto &reg : rvalues) {
          values.push_back(reg.rvalue);
        }
        output[tstr][cstr][lookup_device_type(dev.dtype.type)] = json(values);
      }
    }

    req = RPCMsg("fwmodule.configurevfesfromsinglefile");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.configurevfesfromsinglefile" <<std::endl;

//        std::ifstream* f1 = new std::ifstream("/mnt/ssda/nfs/elm_shared/rpc_test/config_files/vfes/vfes_config.json");
//        std::ifstream* f1 = new std::ifstream("all_registers.json");
        //std::ifstream* f1 = new std::ifstream("/home/cmsdaq/bcp_controller_decompression/ADCconfig/stat/config_elm/ADC_config_bcp1_t0.json");
//        json data = json::parse(*f1);
//        std::string jsonstring = data.dump(); // From file
        std::string jsonstring = output.dump(); // From DB
//        std::cout<<jsonstring<<std::endl;
        req.set_string("configjsonstring", jsonstring);

        
        std::ifstream* f2 = new std::ifstream("/home/cmsdaq/bcp_controller_decompression/ADCconfig/stat/config_elm/vfes_config_bcp1.json");
        json data2 = json::parse(*f2);
        std::string jsonstring2 = data2.dump();
        req.set_string("catiajsonstring", jsonstring2);

        req.set_word("towernb", 0);

        rsp = rpc.call_method(req);
    }
    STANDARD_CATCH;


#endif


    return 0;
}
