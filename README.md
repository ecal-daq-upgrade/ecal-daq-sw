# ecal-daq-sw

## XDAQ Tutorial page
[Tutorial](https://gitlab.cern.ch/cmsos/core/-/wikis/2nd%20Generation%20Tutorial)

## K8s XDAQ Tutorial
[K8s Tutorial](https://gitlab.cern.ch/cmsos/kube/-/wikis/K8s%20Hello%20Tutorial)

## XDAQ RPM Repo Alma9
At the moment we are using alma9 RPMs from master branch:
[core](https://cmsos.web.cern.ch/repo/development/core/master/alma9/x86_64/RPMS/)
[worksuite](https://cmsos.web.cern.ch/repo/development/worksuite/master/alma9/x86_64/RPMS/)

## ECAL Software presentations
[Software schema](https://docs.google.com/presentation/d/1HHXnyb01CGAGlJ-zlNUwI43cV1DUatafT3IFhaA3wYU/)

## Our test VM with AlmaLinux 9
https://openstack.cern.ch/project/

root@xdaqphase2

188.185.64.156

Portainer (if installed):
https://188.185.64.156:9443
daq23

## SSH directly to xdaqphase2
```sh
Host testxdaq
    # Specify the remote username if different from your local one
    User root
    # Use SSHv2 only
    Protocol 2
    # Forward you SSH key agent so that it can be used on further hops
    ForwardAgent yes
    # For X11
    #ForwardX11 yes
    #ForwardX11Trusted no
    # Go through lxplus so that it works from wherever you are, but remember to write the name of key file you have on lxplus
    ProxyCommand ssh -T -q -o 'ForwardAgent yes' lxplus 'ssh-add ~/.ssh/id_ed25519 && nc %h %p'
    #ProxyCommand ssh lxplus

    # Setup a SOCKS5 proxy on local port 2111
    DynamicForward 2111
```

## Questions for XDAQ team
Sending complex arguments at POST request (dictionaries, lists, jsons...) and unpack on server side: send as a string and then convert? is nlohmann::json a good solution or there is a "xdaq wrapper"? should we use xdata::DynamicBag objects to collect data, then a xdata::json::Serializer to make them json (example topics/transfer)?
```cpp
std::string data{response.getArg("message")}; // message is a json
nlohmann::json myjson;
myjson = nlohmann::json::parse(data);
LOG4CPLUS_INFO(this->getApplicationLogger(), "Json dump: " <<myjson.dump());
// replace one of the values in the json
myjson[one_of_the_keys] = 40001; 
// send the json back as a string 
return r.createResponse(myjson.dump(), api::restate::Utils::http_ok, "text/html;charset=UTF-8")
```

What are the main advantages of using example "Metrics" instead of a much simpler "Topics" when we need to publish metrics (temperature, state) of an application?
From the subscriber example: does it make sense to get the data in this way, parse them to string, cut for the size (otherwise you will see extra chars from previous messages) and transform to json?
```cpp
void examples::topics::subscriber::Application::onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref)
{
        LOG4CPLUS_INFO(this->getApplicationLogger(), "received message for topic '" << topic << "' from bus '" << bus->name() << "' of len " << ref->getDataSize());
        auto addr = static_cast<char*>(ref->getDataLocation());
        std::string mystring = addr;
        auto substring = mystring.substr(0,ref->getDataSize());
        LOG4CPLUS_INFO(this->getApplicationLogger(), substring);
        nlohmann::json myjson;
        myjson = nlohmann::json::parse(substring);
        LOG4CPLUS_INFO(this->getApplicationLogger(),myjson["_data"]["temperature"]);
        ref->release();
}
```

## Install XDAQ on VM
From this site [2nd Generation Tutorial](https://gitlab.cern.ch/cmsos/core/-/wikis/2nd%20Generation%20Tutorial)
Following points: 1 (key pairs created and added using openstack tool), 2 ,3
Then, as suggested, moving to [Getting Started](https://gitlab.cern.ch/cmsos/core/-/wikis/Getting%20Started) to install xdaq core and worksuite.
In the instructions, check that the file /etc/yum.repos.d/cmsos.repo uses almalinux 9 and master branch (see RPM repos above)
Prepared a file containing the following exports:
```sh
export XDAQ_ROOT=/opt/xdaq
export LD_LIBRARY_PATH=/opt/xdaq/lib/:/opt/xdaq/lib64
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export XDAQ_ARCH=`uname -p`
export XDAQ_CHECKOS=`$XDAQ_ROOT/build/checkos.sh`
export XDAQ_PLATFORM=`echo ${XDAQ_ARCH}_${XDAQ_CHECKOS}`
```

and tested xdaq doing:
```sh
source thesourcefile.sh
/opt/xdaq/bin/xdaq
```
Back to [2nd Generation Tutorial](https://gitlab.cern.ch/cmsos/core/-/wikis/2nd%20Generation%20Tutorial) documentation, you may want to follow point 4
If you want to have a look to the examples you can
```sh
git clone https://gitlab.cern.ch/cmsos/core.git
cd core
git checkout master
```
Then following tutorial for some of the examples and they should work fine.
In same cases you have to redefine in the yaml file the position of the lib file you have just compiled

## JIRA Tickets
Some JIRA tickets I have open with questions about the code:

- https://its.cern.ch/jira/browse/CMSOS-227
- https://its.cern.ch/jira/browse/CMSOS-228
- https://its.cern.ch/jira/browse/CMSOS-229
- https://its.cern.ch/jira/browse/CMSOS-230
- https://its.cern.ch/jira/browse/CMSOS-231

## Problems and solutions
Mosquito error
```
mosquitto interface has disconnected from bus mosquitto with reason 14
```
You have forgotten to launch
```
mosquitto -d
```
Maybe the process is dead or you have rebooted the pc

## How to test our Software
### Metrics test
```sh
git clone https://gitlab.cern.ch/ecal-daq-upgrade/ecal-daq-sw.git
cd ecal-daq-sw
git checkout e5f5c40 (these instructions are not included in this commit)
source xdaq_env.sh
cd fsm
make
```

Than in two separe terminals
```sh
source /root/ecal-daq-sw/xdaq_env.sh
cd /root/ecal-daq-sw/fsm/tests/metrics_test
/opt/xdaq/bin/xdaq --profile ecalsup.yml --host 127.0.0.1 --port 80 --level DEBUG --set confFile=app_conf.yml --set appId=ECALSupervisor
```
and
```sh
source /root/ecal-daq-sw/xdaq_env.sh
cd /root/ecal-daq-sw/fsm/tests/metrics_test
/opt/xdaq/bin/xdaq --profile bcpsup.yml --host 127.0.0.1 --port 82 --level DEBUG --set confFile=app_conf.yml --set appId=BCP1
```
you can launch how many BCPSupervisor applications you want, just open another terminal and repeat the second block changing each time ```port``` and ```appId``` in the command string and adding a new element in the ```app_conf.yml``` file.

You need a mosquitto broker running on the machine. On a terminal in the machine you can launch 
```
mosquitto -d
mosquitto_sub -h localhost -t 'metris/device/error' -q 2
```
So you have the broker and you can listen to the messages sent by the applications. Messages will appear only when the applications will be in ```Running``` state.

Then to change the state of the system from Initial to Halt you can launch from the xdaqphase2 machine:
```sh
curl 127.0.0.1:80/changestate/gohalt
curl 127.0.0.1:80/changestate/gorunning
```

### Metris and Mosquitto publish test
```sh
git clone https://gitlab.cern.ch/ecal-daq-upgrade/ecal-daq-sw.git
cd ecal-daq-sw
git checkout 87f2e1a (these instructions are not included in this commit)
source xdaq_env.sh
cd fsm
make
```

You need two mosquitto brokers running on the machine. One is for the metris and one is for the extra topic. On on terminals in the machine you can launch:
```
mosquitto -d
mosquitto -d -c tests/metris_and_sub_test/mosquitto.conf
mosquitto_sub -h localhost -t 'metris/device/error' -q 2
mosquitto_sub -h localhost -p 1884 -t 'state' -q 2 (to be launched on a second terminal)
```
In this example on broker with 'metris/device/error', you can listen to the messages sent by the applications. Messages will appear only when the applications will be in ```Running``` state. Both ECAL and BCP Supervisors will write
On broker with 'state', you can listen to the new state messages sent by the applications. Messages are sent only by the ECALSupervisor because it is not overwriting the stateChanged function of the Supervisor class.

Than in two separe terminals
```sh
source /root/ecal-daq-sw/xdaq_env.sh
cd /root/ecal-daq-sw/fsm/tests/metris_and_sub_test
/opt/xdaq/bin/xdaq --profile ecalsup.yml --host 127.0.0.1 --port 80 --level DEBUG --set confFile=app_conf.yml --set appId=ECALSupervisor
```
and
```sh
source /root/ecal-daq-sw/xdaq_env.sh
cd /root/ecal-daq-sw/fsm/tests/metris_and_sub_test
/opt/xdaq/bin/xdaq --profile bcpsup.yml --host 127.0.0.1 --port 82 --level DEBUG --set confFile=app_conf.yml --set appId=BCP1
```

Then to change the state of the system from Initial to Halt you can launch from the xdaqphase2 machine:
```sh
curl 127.0.0.1:80/changestate/gohalt
curl 127.0.0.1:80/changestate/gorunning
```

**You don't really need two brokers for this test. The first mosquitto you launch, can sustain both metris and state channels (or topics). You can repeat this test changing port 1884 to 1883 in the yaml file, launching only the first mosquitto and using two similar mosquitto_sub just listening to different topics.**

### Full Docker example

Starting from commit ```ce9a4ee``` we have included in the Dockerfile XDAQ image:
```
FROM gitlab-registry.cern.ch/cmsos/hub/cmsos-master-alma9-x64-full:1.9.0.0
```
We can get this image only via git token released for ECAL from the XDAQ team.
See ticket: https://its.cern.ch/jira/browse/CMSOS-255
The git token is available in the ```passdb``` file.
First build the image we are going to use
```
cd /root/ecal-daq-sw/
docker build -t xdaq_dockeronly -f docker_tests/docker_only/Dockerfile .
```
This command may require first a:
```
docker login gitlab-registry.cern.ch
```
Now on a terminal you can launch the mosquitto broker ina docker EMQX image
```
docker stop emqxcont
docker rm emqxcont
docker run --net our-network --interactive --tty -p 18083:18083 -p 1883:1883 --name emqxcont gitlab-registry.cern.ch/cmsos/buildah/emqx:5.1.3
```
Communication problems between the broker and the apps have been risolved following suggestions from ticket https://its.cern.ch/jira/browse/CMSOS-254.

On two other terminals
```
docker run -p 80 --name cont_ecal --net our-network -i gitlab-registry.cern.ch/ecal-daq-upgrade/ecal-daq-sw:latest
docker run --name cont_bcp1 --net our-network -i gitlab-registry.cern.ch/ecal-daq-upgrade/ecal-daq-sw:latest --profile docker_tests/docker_only/bcpsup.yml --set appId=BCP1 --set confFile=docker_tests/docker_only/app_conf.yml --level DEBUG --host 127.0.0.1 --port 80
```

Then you can use the usual commands
```
curl http://172.18.0.3:80/changestate/gohalt
curl http://172.18.0.3:80/changestate/gorunning
```

You can subscribe to the metris sent by the apps with
```
mosquitto_sub -h localhost -t 'metris/device/error' -q 2
```

~~OLD INSTRUCTIONS:
Before having access to update XDAQ images we have tried to create an image with most recent XDAQ libraries in this way:

Starting from ecal-daq-sw git commit ```5f7d40d```
First we need to create an image with the correct xdaq libraries (aligned to xdaqphase2 machine). It is related to the problem discussed in this ticket (https://its.cern.ch/jira/browse/CMSOS-255) and for the moment we are using this method:
```
ssh xdaqphase2
cd /root/ecal-daq-sw
docker build -t xdaq_base -f docker_tests/docker_only/Dockerfile.test .
```
This will create a image xdaq_base we can use in the next builds of our code

Now we can build the image with our code and we will put it on our git repository
```
docker build -t gitlab-registry.cern.ch/ecal-daq-upgrade/ecal-daq-sw -f docker_tests/docker_only/Dockerfile .
```
END OLD INSTRUCTIONS~~


## OLD INSTRUCTIONS - TO CHECK or delete
~~### Docker compose
Better way to run the three containers is using a docker-compose file like this one:
```yaml
version: "3.9"
services:
  emqx:
    image: gitlab-registry.cern.ch/cmsos/buildah/emqx:5.1.3
    container_name: emqx_cont
    ports:
      - 18083:18083
    healthcheck:
      test: ["CMD", "/opt/emqx/bin/emqx_ctl", "status"] # NOT WORK
      interval: 5s
      timeout: 25s
      retries: 5

  ecalsupervisor:
    image: xdaqimage:latest
    container_name: ecal_cont
    ports:
      - 80:80
    depends_on:
      emqx:
        condition: service_healthy

  bcpsupervisor:
    image: xdaqimage:latest
    container_name: bcp_cont
    command:  --profile yaml/bcpsup.yml --set inputGlobal=5
    depends_on:
      emqx:
        condition: service_healthy
```
It seems that you don't need to define a private network... docker-compose inserts them automatically inside a new one.
Also this version supposes that you have compiled the code before launching docker compose command:
```
docker compose up
```
The evolution from here would be Kubernetes and Helm because in a docker compose file you cannot create multiple containers of the same image programmatically.~~

