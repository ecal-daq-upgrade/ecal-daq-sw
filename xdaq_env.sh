export XDAQ_ROOT=/opt/xdaq
export LD_LIBRARY_PATH=/opt/xdaq/lib/:/opt/xdaq/lib64
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export XDAQ_ARCH=`uname -p`
export XDAQ_CHECKOS=`$XDAQ_ROOT/build/checkos.sh`
export XDAQ_PLATFORM=`echo ${XDAQ_ARCH}_${XDAQ_CHECKOS}`
