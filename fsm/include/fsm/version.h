#ifndef _fsm_version_h_
#define _fsm_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define MYEXAMPLES_FSM_VERSION_MAJOR 1
#define MYEXAMPLES_FSM_VERSION_MINOR 0
#define MYEXAMPLES_FSM_VERSION_PATCH 0
// If any previous versions available E.g. #define MYEXAMPLES_FSM_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef MYEXAMPLES_FSM_PREVIOUS_VERSIONS


//
// Template macros
//
#define MYEXAMPLES_FSM_VERSION_CODE PACKAGE_VERSION_CODE(MYEXAMPLES_FSM_VERSION_MAJOR,MYEXAMPLES_FSM_VERSION_MINOR,MYEXAMPLES_FSM_VERSION_PATCH)
#ifndef MYEXAMPLES_FSM_PREVIOUS_VERSIONS
#define MYEXAMPLES_FSM_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(MYEXAMPLES_FSM_VERSION_MAJOR,MYEXAMPLES_FSM_VERSION_MINOR,MYEXAMPLES_FSM_VERSION_PATCH)
#else
#define MYEXAMPLES_FSM_FULL_VERSION_LIST  MYEXAMPLES_FSM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(MYEXAMPLES_FSM_VERSION_MAJOR,MYEXAMPLES_FSM_VERSION_MINOR,MYEXAMPLES_FSM_VERSION_PATCH)
#endif

namespace fsm
{
  const std::string project = "ecal-daq-sw";
  const std::string package  =  "fsm";
  const std::string versions = MYEXAMPLES_FSM_FULL_VERSION_LIST;
  const std::string summary = "A simple XDAQ application";
  const std::string description = "Example of a FSM application triggered by api";
  const std::string authors = "Me";
  const std::string link = "no git repo yet";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies() ;
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

