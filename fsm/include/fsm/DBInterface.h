#ifndef fsm_dbinterface_h_
#define fsm_dbinterface_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "api/restate/Interface.h"
#include "xdata/String.h"
using namespace bcp;
using namespace bcp::myoracle;

namespace fsm {
  class DBInterface: public xdaq::Application, public api::restate::ServiceListener, public xdata::ActionListener
 {
	  public:

      XDAQ_INSTANTIATOR();
      DBInterface(xdaq::ApplicationStub * s);

      // As xdata::ActionListener we need function actionPerformed. 
      // Remember to define it in the .cc even if empty otherwise you will have 
      // a very generic error at run time
      void actionPerformed(xdata::Event& event);



      const std::shared_ptr<api::restate::Response> ping(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> changeState(const api::restate::Request& r);
      //const std::shared_ptr<api::restate::Response> getLatestVFEConfig(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> getLatestVFEConfigId(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> getVFEConfigById(const api::restate::Request& r);

      xdata::String appId_;
      xdata::String userName_;
      xdata::String password_;
      xdata::String schemaHost_;
      xdata::String schemaService_;
      xdata::String dbInterface_;

      bcpdb_oracle oracleInterface_;
  };
};


#endif

