#ifndef fsm_bcpsupervisor_h_
#define fsm_bcpsupervisor_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "api/restate/Interface.h"
#include "MyFsm.h"
#include "Supervisor.h"
#include "toolbox/mem/Pool.h"
#include "api/topics/Member.h"
#include "api/topics/MemberListener.h"
#include "api/topics/Bus.h"


namespace fsm {
  class BCPSupervisor: public fsm::Supervisor {
    public:

      XDAQ_INSTANTIATOR();
      BCPSupervisor(xdaq::ApplicationStub * s);
      const std::shared_ptr<api::restate::Response> responseTest(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> getAllRegisters(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> changeState(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> configureOnDetectorById(const api::restate::Request& r); 
      void stateChanged (toolbox::fsm::FiniteStateMachine & fsm);
      void parseConfYaml();
      
      std::string ECALSupervisorAddress_;
      std::string DBInterfaceAddress_;
      std::string ELMAddress_;

      void onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref);
      void onConnect(std::shared_ptr<api::topics::Bus> bus);
      void updateFunction();
      std::shared_ptr<api::topics::Bus> pubsub_bus_;

      //overridden MyFsm function
      void fromHtoCAction (toolbox::Event::Reference e);
      void fromCtoRAction (toolbox::Event::Reference e);
      void failed(toolbox::Event::Reference e);

      void configureOnDetector(std::string conf_id, std::string towers);
  };
};


#endif

