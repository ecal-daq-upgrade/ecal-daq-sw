#ifndef fsm_myfsm_h_
#define fsm_myfsm_h_

#include "toolbox/fsm/FiniteStateMachine.h"
namespace fsm {

	class MyFsm {
    public:

      MyFsm();
 
      virtual void fromItoHAction (toolbox::Event::Reference e);
      virtual void fromHtoCAction (toolbox::Event::Reference e);
      virtual void fromCtoRAction (toolbox::Event::Reference e);
      virtual void fromRtoCAction (toolbox::Event::Reference e);
      virtual void fromCtoHAction (toolbox::Event::Reference e);

   		// User callback invoked when failed state is entered
			virtual void failed (toolbox::Event::Reference e);

			// callback invoked when state transition occurs
   		virtual void stateChanged (toolbox::fsm::FiniteStateMachine & fsm);

   		// callback invoked when failed state is entered
   		void inFailed (toolbox::fsm::FiniteStateMachine & fsm);

      toolbox::fsm::State getNextState (toolbox::fsm::State s, std::string evtName);
      void fireChangeState(std::string action);

		protected:
      toolbox::fsm::FiniteStateMachine fsm_;

  };
};

#endif
