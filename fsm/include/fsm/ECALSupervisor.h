#ifndef fsm_ecalsupervisor_h_
#define fsm_ecalsupervisor_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "api/restate/Interface.h"
#include "MyFsm.h"
#include "Supervisor.h"
#include "toolbox/mem/Pool.h"
//#include "api/topics/Member.h"
//#include "api/topics/MemberListener.h"
//#include "api/topics/Bus.h"


namespace fsm {
  class ECALSupervisor: public fsm::Supervisor {
    public:

      XDAQ_INSTANTIATOR();
      ECALSupervisor(xdaq::ApplicationStub * s);

      const std::shared_ptr<api::restate::Response> responseTest(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> changeState(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> sendStateChanged(const api::restate::Request& r);
      void broadcastChangeState();
      void sendChangeStateRequest(std::string bcpAddress);
//      void onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref);

      // Override functions of Supervisor class
//      void onConnect(std::shared_ptr<api::topics::Bus> bus);

//      std::shared_ptr<api::topics::Bus> pubsub_bus_;
//      std::map<std::string, std::string> supervisorStateList_;

//      void actionPerformed(xdata::Event& e);
      void parseConfYaml();
      void waitForStateChanged();
      std::map<std::string,std::string> bcpSupervisorsAddresses_;
      std::map<std::string,std::string> bcpSupervisorsStates_;
      bool waitingAppStateChange_;
      std::string change_state_cmd_;
      void updateFunction();
/*      void loop();
      void publishCurrentState(std::shared_ptr<api::topics::Bus> mybus, toolbox::mem::Pool * mypool);
      void actionPerformed(xdata::Event& event);
      void onConnect(std::shared_ptr<api::topics::Bus> bus);
      void onDisconnect(std::shared_ptr<api::topics::Bus> bus);
      void onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref);

      std::map<std::string, std::string> supervisorStateList_;  
 
    protected:
      
      std::shared_ptr<api::topics::Member> pubsub_member_;
      toolbox::mem::Pool * pubsub_pool_;
      std::shared_ptr<api::topics::Bus> pubsub_bus_;
			std::atomic<bool> connected_;
      std::shared_ptr<api::topics::Bus> pubsub_bus2_;

      //overridden MyFsm function*/
      void fromItoHAction (toolbox::Event::Reference e);
      void fromHtoCAction (toolbox::Event::Reference e);
      void fromCtoRAction (toolbox::Event::Reference e);
      void fromRtoCAction (toolbox::Event::Reference e);
      void fromCtoHAction (toolbox::Event::Reference e);

	};
};


#endif

