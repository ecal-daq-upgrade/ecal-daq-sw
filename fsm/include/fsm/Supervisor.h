#ifndef fsm_supervisor_h_
#define fsm_supervisor_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "api/restate/Interface.h"
#include "MyFsm.h"
#include "toolbox/mem/Pool.h"
#include "api/topics/Member.h"
#include "api/topics/MemberListener.h"
#include "api/topics/Bus.h"
#include "xdata/String.h"

#include "api/metris/Service.h"
#include "api/metris/ServiceListener.h"
#include "api/metris/Metrics.h"
#include "xdata/DynamicBag.h"
#include "api/metris/Interface.h"




namespace fsm {
  class Supervisor: public xdaq::Application, public api::restate::ServiceListener, public xdata::ActionListener, public api::topics::MemberListener, public fsm::MyFsm, public api::metris::ServiceListener
 {
	  public:

      XDAQ_INSTANTIATOR();
      Supervisor(xdaq::ApplicationStub * s);
      virtual const std::shared_ptr<api::restate::Response> changeState(const api::restate::Request& r);
      const std::shared_ptr<api::restate::Response> responseTestParent(const api::restate::Request& r);
      void loop();


      // As xdata::ActionListener we need function actionPerformed. 
      // Remember to define it in the .cc even if empty otherwise you will have 
      // a very generic error at run time
      void actionPerformed(xdata::Event& event);
      virtual void onConnect(std::shared_ptr<api::topics::Bus> bus);
      virtual void onDisconnect(std::shared_ptr<api::topics::Bus> bus);
      virtual void stateChanged (toolbox::fsm::FiniteStateMachine & fsm);      
      std::shared_ptr<api::topics::Member> state_member_;
      std::shared_ptr<api::topics::Bus>  bus_;
      virtual void parseConfYaml();

      //overridden MyFsm function
      virtual void fromItoHAction (toolbox::Event::Reference e);
      virtual void fromHtoCAction (toolbox::Event::Reference e);
      virtual void fromCtoRAction (toolbox::Event::Reference e);
      virtual void fromRtoCAction (toolbox::Event::Reference e);
      virtual void fromCtoHAction (toolbox::Event::Reference e);
      virtual void failed (toolbox::Event::Reference e);
      virtual void updateFunction();
//
      xdata::String confFile_;
      xdata::String appId_;
      bool updateEnable_;

      std::shared_ptr<api::metris::Service> service_;
	    std::shared_ptr<api::metris::Metrics<xdata::DynamicBag>> channel_error_;
      std::shared_ptr<api::topics::Member> member_;

  };
};


#endif

