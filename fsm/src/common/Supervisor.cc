#include "fsm/Supervisor.h"
#include "api/restate/Utils.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "api/topics/Interface.h"
#include "api/topics/exception/Exception.h"


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/net/URN.h"
#include "chrono"

XDAQ_INSTANTIATOR_IMPL(fsm::Supervisor);

fsm::Supervisor::Supervisor(xdaq::ApplicationStub * s): 
  xdaq::Application(s), 
  MyFsm()
  {

    // Initialise MQTT pubsub
//    toolbox::net::URN pubsub_urn("topics", "pubsub");
//    pubsub_pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(pubsub_urn, new toolbox::mem::CommittedHeapAllocator(0x100000));
//    pubsub_member_ = api::topics::getInterface(this->getApplicationContext())->join(this, this, pubsub_pool_);
//    pubstate_bus_ = pubsub_member_->busAttach("pubstate_bus");

//    std::thread source_thread(&fsm::Supervisor::loop, this);
//    source_thread.detach();

    state_member_ = api::topics::getInterface(this->getApplicationContext())->join(this, this, nullptr);
    bus_ = state_member_->busAttach("state_bus");

    LOG4CPLUS_INFO(this->getApplicationLogger(), "In Constructor...");
    auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);

    member->registerResource("/changestate/{changestatecmd}", std::bind(&fsm::Supervisor::changeState, this, std::placeholders::_1));
    member->registerResource("/responsetestparent", std::bind(&fsm::Supervisor::responseTestParent, this, std::placeholders::_1));
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->fireItemAvailable("confFile", &confFile_);
    this->getApplicationInfoSpace()->fireItemAvailable("appId", &appId_);

    updateEnable_ = true;
}

const std::shared_ptr<api::restate::Response> fsm::Supervisor::responseTestParent(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::Supervisor::responseTest");
  return r.createResponse("Hello!", api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}


const std::shared_ptr<api::restate::Response> fsm::Supervisor::changeState(const api::restate::Request& r) {

  std::string change_state_cmd = std::string(r.getArg("changestatecmd"));
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Called changeState with command " << change_state_cmd);

  toolbox::fsm::State nextState = getNextState(fsm_.getCurrentState(), change_state_cmd);
  std::string stateName = fsm_.getStateName(nextState);
  //LOG4CPLUS_INFO(this->getApplicationLogger(), "About to broadcast following state: " << stateName);

  std::string response = "Event accepted";
  this->fireChangeState(change_state_cmd);
  return r.createResponse(response, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}

void fsm::Supervisor::onConnect(std::shared_ptr<api::topics::Bus> bus) {
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::onConnect which is virtual, do you want to override this function?");
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "bus " << bus->name() << " is connected");
}

void fsm::Supervisor::onDisconnect(std::shared_ptr<api::topics::Bus> bus) {
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::onDisconnect which is virtual, do you want to override this function?");
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "bus " << bus->name() << " has diconnected");
}

/*void fsm::Supervisor::publishCurrentState(std::shared_ptr<api::topics::Bus> mybus, toolbox::mem::Pool * mypool) {
  std::string currentState = fsm_.getStateName(fsm_.getCurrentState());
  toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(mypool,2048);
  auto addr = static_cast<char*>(ref->getDataLocation());
  std::snprintf(addr, currentState.length()+1, "%s", currentState.c_str());
  ref->setDataSize(currentState.length());
  // No check if bus is connect... ok?
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Publishing state " << currentState << " on topic " << "appstate/"+appId_);
  mybus->publish("appstate/"+appId_,ref);
}*/


// Loop function used to publish app state + heartbeat
void fsm::Supervisor::loop() {

  int counter = 0;
  while(1)
  {
    //this->publishCurrentState(pubstate_bus_, pubsub_pool_);
    std::chrono::milliseconds timespan(5000);
    std::this_thread::sleep_for(timespan);
    counter ++;
    std::string currentState = fsm_.getStateName(fsm_.getCurrentState());
    //if (counter == 5) XCEPT_RAISE(xdaq::exception::Exception, "random error after x seconds");
    if (updateEnable_ && currentState == "Running") updateFunction();
  }
}

// callback invoked when state transition occurs
void fsm::Supervisor::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "New state " << fsm.getStateName(fsm.getCurrentState()) << " reached, publishing it...");
  //this->publishCurrentState(pubstate_bus_, pubsub_pool_);
  //bus_->publish("state", "State changed to:"+fsm.getStateName(fsm.getCurrentState()));
}

void fsm::Supervisor::parseConfYaml() {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "We are in fsm::Supervisor::parseConfYaml which is virtual, do you want to override this function?");
}

// Why do we need this function?
void fsm::Supervisor::actionPerformed(xdata::Event& event) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "We are in fsm::Supervisor::actionPerformed which is virtual, do you want to override this function?");
   service_ = api::metris::getInterface(this->getApplicationContext())->join(this, this);	
   channel_error_ = service_->createMetrics<xdata::DynamicBag>("device/error", {}, {});
   std::thread source_thread(&fsm::Supervisor::loop, this); 
   source_thread.detach();
   parseConfYaml();

}

void fsm::Supervisor::fromItoHAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::fromItoHAction which is virtual, do you want to override this function?");
}

void fsm::Supervisor::fromHtoCAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::fromHtoCAction which is virtual, do you want to override this function?");
}

void fsm::Supervisor::fromCtoRAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::fromCtoRAction which is virtual, do you want to override this function?");
}

void fsm::Supervisor::fromRtoCAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::fromRtoCAction which is virtual, do you want to override this function?");
}

void fsm::Supervisor::fromCtoHAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::fromCtoHAction which is virtual, do you want to override this function?");
}

void fsm::Supervisor::failed(toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::failed which is virtual, do you want to override this function?");
  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Failure occurred when performing transition from: " << fe.getFromState() <<  " to: " << fe.getToState());
  LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Exception: " << fe.getException().what());
}

void fsm::Supervisor::updateFunction() {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::Supervisor::updateFunction which is virtual, do you want to override this function?");
 	channel_error_->put({{"temperature", (float)rand() },{"units", "U+2103"}});
  
}
