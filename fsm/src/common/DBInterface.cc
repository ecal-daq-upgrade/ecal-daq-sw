#include "dbinterface.h"
#include "dboracle.h"
#include "fsm/DBInterface.h"
#include "api/restate/Utils.h"

using namespace bcp;
using namespace bcp::myoracle;

XDAQ_INSTANTIATOR_IMPL(fsm::DBInterface);

fsm::DBInterface::DBInterface(xdaq::ApplicationStub * s): 
  xdaq::Application(s) 
  {

    LOG4CPLUS_INFO(this->getApplicationLogger(), "In Constructor...");
    auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);

    member->registerResource("/ping", std::bind(&fsm::DBInterface::ping, this, std::placeholders::_1));
    member->registerResource("/changestate/{changestatecmd}", std::bind(&fsm::DBInterface::changeState, this, std::placeholders::_1));
    //member->registerResource("/getLatestVFEConfig", std::bind(&fsm::DBInterface::getLatestVFEConfig, this, std::placeholders::_1));
    member->registerResource("/getLatestVFEConfigId", std::bind(&fsm::DBInterface::getLatestVFEConfigId, this, std::placeholders::_1));
    member->registerResource("/getVFEConfigById/{conf_id}", std::bind(&fsm::DBInterface::getVFEConfigById, this, std::placeholders::_1));

    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->fireItemAvailable("appId", &appId_);
    this->getApplicationInfoSpace()->fireItemAvailable("userName", &userName_);
    this->getApplicationInfoSpace()->fireItemAvailable("password", &password_);
    this->getApplicationInfoSpace()->fireItemAvailable("schemaHost", &schemaHost_);
    this->getApplicationInfoSpace()->fireItemAvailable("schemaService", &schemaService_);
    this->getApplicationInfoSpace()->fireItemAvailable("dbInterface", &dbInterface_); 
    // I cannot launch open_oracle_connection here because env variables are not define yet
    // I have to move the code in actionPerformed for example
    // open_oracle_connection();

}

// Why do we need this function?
void fsm::DBInterface::actionPerformed(xdata::Event& event) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "We are in fsm::DBInterface::actionPerformed");

  LOG4CPLUS_INFO(this->getApplicationLogger(), "Opening connection to DB");
  // Here we should have the env variables defined. We can open the DB.
  if(dbInterface_ == "oracle") {
   
    std::string connectString = "(DESCRIPTION = (ADDRESS=(PROTOCOL = TCP)(HOST = " + schemaHost_.toString() + ")(PORT = 10121)) (CONNECT_DATA= (SERVICE_NAME = " + schemaService_.toString() + ") (SERVER = DEDICATED)) )";
    std::string name = userName_.toString();
    std::string password = password_.toString();
    oracleInterface_ = bcpdb_oracle(&this->getApplicationLogger(), name, password, connectString);
    oracleInterface_.memoryMode();
  }

}


const std::shared_ptr<api::restate::Response> fsm::DBInterface::ping(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Here in fsm::DBInterface::ping with id "<<appId_.toString());
  
  return r.createResponse("Pong!", api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}


const std::shared_ptr<api::restate::Response> fsm::DBInterface::changeState(const api::restate::Request& r) {

  std::string change_state_cmd = std::string(r.getArg("changestatecmd"));
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Called changeState with command " << change_state_cmd);

  std::string response = "Event accepted";
  return r.createResponse(response, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}

const std::shared_ptr<api::restate::Response> fsm::DBInterface::getLatestVFEConfigId(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Here in fsm::DBInterface::getLatestVFEConfigId with app id "<<appId_.toString());

  int last_id = -999;
  if(dbInterface_ == "oracle") {
    last_id = oracleInterface_.getLatestVFEConfigId();
  }
  return r.createResponse(std::to_string(last_id), api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}


const std::shared_ptr<api::restate::Response> fsm::DBInterface::getVFEConfigById(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Here in fsm::DBInterface::getVFEConfigById with app id "<<appId_.toString());

  json full_config;
  if(dbInterface_ == "oracle") {
    int conf_id = stoi(std::string(r.getArg("conf_id")));
    json vfe_config_ids = oracleInterface_.getVFEConfig(conf_id);
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), vfe_config_ids.dump());
    oracleInterface_.getAdcs(vfe_config_ids["adc_set_id"], full_config);
    // Then we need to prepare the defaults first
    oracleInterface_.getCatiaDefaults(vfe_config_ids["catia_def_set_id"], full_config);
    oracleInterface_.getDtuDefaults(vfe_config_ids["dtu_def_set_id"], full_config);
    // Only after the defaults we can do this action
    oracleInterface_.getPedestals(vfe_config_ids["pedestal_set_id"], full_config);
    LOG4CPLUS_INFO(this->getApplicationLogger(), "lpgbt_def_set_id " << vfe_config_ids["lpgbt_def_set_id"]);
    oracleInterface_.getLpgbtDefaults(vfe_config_ids["lpgbt_def_set_id"], full_config);

  }

  return r.createResponse(full_config.dump(), api::restate::Utils::http_ok, "text/html;charset=UTF-8");

}
/*
const std::shared_ptr<api::restate::Response> fsm::DBInterface::getLatestVFEConfig(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Here in fsm::DBInterface::getLatestVFEConfig with app id "<<appId_.toString());

  json full_config;
  if(dbInterface_ == "oracle") {
    int lastId = oracleInterface_.getLatestVFEConfigId();
    json vfe_config_ids = oracleInterface_.getVFEConfig(lastId);
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), vfe_config_ids.dump());
    oracleInterface_.getAdcs(vfe_config_ids["adc_set_id"], full_config);
    // Then we need to prepare the defaults first
    oracleInterface_.getCatiaDefaults(vfe_config_ids["catia_def_set_id"], full_config);
    oracleInterface_.getDtuDefaults(vfe_config_ids["dtu_def_set_id"], full_config);
    // Only after the defaults we can do this action
    oracleInterface_.getPedestals(vfe_config_ids["pedestal_set_id"], full_config);
    LOG4CPLUS_INFO(this->getApplicationLogger(), "lpgbt_def_set_id " << vfe_config_ids["lpgbt_def_set_id"]);
    oracleInterface_.getLpgbtDefaults(vfe_config_ids["lpgbt_def_set_id"], full_config);
    
  }

  return r.createResponse(full_config.dump(), api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}
*/
