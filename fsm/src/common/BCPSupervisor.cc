#include "fsm/BCPSupervisor.h"
#include "api/restate/Utils.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "api/topics/Interface.h"
#include "api/topics/exception/Exception.h"

#include "xdata/DynamicBag.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/net/URN.h"
#include "nlohmann/json.hpp"
#include "xdata/json/Serializer.h"
#include "yaml-cpp/yaml.h"
#include "dboracle.h"
#include "rpctest.h"
using namespace bcp::myoracle;

XDAQ_INSTANTIATOR_IMPL(fsm::BCPSupervisor);

fsm::BCPSupervisor::BCPSupervisor(xdaq::ApplicationStub * s): fsm::Supervisor(s) {

  LOG4CPLUS_INFO(this->getApplicationLogger(), "Hello World from BCPSup!");

  // Initialise MQTT pubsub
  //auto sub_cb = std::bind(&fsm::BCPSupervisor::onMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
  //pubsub_bus_ = pubsub_member_->busAttach("mosquitto", sub_cb);
  auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
  member->registerResource("/responsetestchild", std::bind(&fsm::BCPSupervisor::responseTest, this, std::placeholders::_1));
  member->registerResource("/getallregisters/{towers}", std::bind(&fsm::BCPSupervisor::getAllRegisters, this, std::placeholders::_1));
  member->registerResource("/configureOnDetectorById/{conf_id}/{towers}", std::bind(&fsm::BCPSupervisor::configureOnDetectorById, this, std::placeholders::_1));
  LOG4CPLUS_INFO(this->getApplicationLogger(), "getURN " << getApplicationDescriptor()->getURN());
  LOG4CPLUS_INFO(this->getApplicationLogger(), "HELLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
}

const std::shared_ptr<api::restate::Response> fsm::BCPSupervisor::responseTest(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::BCPSupervisor::responseTest");
  return r.createResponse("Hello!", api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}

const std::shared_ptr<api::restate::Response> fsm::BCPSupervisor::getAllRegisters(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::BCPSupervisor:::getallregisters");
  RPCSvc rpc;
  openRpcConnection(rpc, "pcminn34");
  std::string towers = std::string(r.getArg("towers"));
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::BCPSupervisor:: ABOUT TO APPEND TOWERS"<<towers);
  std::string jsonstring;
  getallregisters(this->getApplicationLogger(), rpc, towers, jsonstring);
  return r.createResponse(jsonstring, api::restate::Utils::http_ok, "application/json; charset=utf-8");
}



void fsm::BCPSupervisor::onConnect(std::shared_ptr<api::topics::Bus> bus)
{
    LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " is connected, subscribe to topic ");
}

const std::shared_ptr<api::restate::Response> fsm::BCPSupervisor::changeState(const api::restate::Request& r) { 


  std::string response = "Event accepted";
  try {
    this->fireChangeState(std::string(r.getArg("changestatecmd")));
  } catch (const std::exception& e) {
    LOG4CPLUS_INFO(this->getApplicationLogger(), "Error when firing event: "<<e.what());
    response = e.what();	
  }
  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of BCPSupervisor::changeState "<<api::restate::Utils::http_ok);
  return r.createResponse(response, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}

void fsm::BCPSupervisor::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) {

  // We inform the ECALSupervisor that we have reached the new state
  toolbox::fsm::State currentState = fsm_.getCurrentState();
  std::string stateName = fsm_.getStateName(currentState);
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Sending confirmation of new state changed " <<  stateName << " to address " << ECALSupervisorAddress_);
  auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
  auto connection = member->connect(ECALSupervisorAddress_,{});
  std::string message = "/sendstatechanged/"+appId_.toString()+"/"+stateName;
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Sending confirmation before GET");
  connection->get(message); 
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Sending confirmation after GET");
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Sending confirmation done with "+message); 
}

void fsm::BCPSupervisor::failed (toolbox::Event::Reference e) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Sending failed state to the ECALSupervisor");
  auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
  auto connection = member->connect(ECALSupervisorAddress_,{});
  std::string message = "/sendstatechanged/"+appId_.toString()+"/failed";
  connection->get(message);
}

void fsm::BCPSupervisor::parseConfYaml() {
    
    LOG4CPLUS_INFO(this->getApplicationLogger(), "In parseConfYaml with conf file: " + confFile_.toString());
    YAML::Node config = YAML::LoadFile(confFile_.toString());
    YAML::Node apps = config["applications"];
    for (YAML::const_iterator it=apps.begin(); it!=apps.end(); ++it) {
      YAML::Node innerNode= it->second;
      std::string address = innerNode["address"].as<std::string>();
      std::string appClass = innerNode["class"].as<std::string>();
      if (appId_ == it->first.as<std::string>()) {
          ELMAddress_ = innerNode["elm"].as<std::string>();
          LOG4CPLUS_INFO(this->getApplicationLogger(), "BCP Supervisor will talk to the elm at " <<ELMAddress_);
}
      if (appClass == "ECALSupervisor") {
        ECALSupervisorAddress_ = "http://"+address;
        LOG4CPLUS_INFO(this->getApplicationLogger(), "ECALSupervisor address: "+ECALSupervisorAddress_);
      }
      if (appClass == "DBInterface") {
        DBInterfaceAddress_ = "http://"+address;
        LOG4CPLUS_INFO(this->getApplicationLogger(), "DBInterface address: "+DBInterfaceAddress_);
      }
    }
    LOG4CPLUS_INFO(this->getApplicationLogger(), "End of parseConfYaml");
}
/*
void fsm::BCPSupervisor::onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref)
{
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "received message for topic '" << topic << "' from bus '" << bus->name() << "' of len " << ref->getDataSize());
    auto addr = static_cast<char*>(ref->getDataLocation());
    std::string mystring = addr;
    auto substring = mystring.substr(0,ref->getDataSize());
    ref->release();
    LOG4CPLUS_INFO(this->getApplicationLogger(), substring);
    nlohmann::json myjson;
    try {
      myjson = nlohmann::json::parse(substring);
      if (myjson.find("_data") != myjson.end()) {
        LOG4CPLUS_INFO(this->getApplicationLogger(),myjson["_data"]["temperature"]);
      }
    } catch(...) {
      LOG4CPLUS_WARN(this->getApplicationLogger(), "Problem parsing the json, is it a json?");
    }
    if (topic == "changestatetopics") {
      this->fireChangeState(substring);
    }
}*/

void fsm::BCPSupervisor::updateFunction() {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "::updateFunction - Here we should send the state of the electronics");
  xdata::DynamicBag transfer;
  xdata::json::Serializer jserializer;
  nlohmann::json doc;
  int totalTowerNumber = 3;
  for (int towerIndex = 0; towerIndex < totalTowerNumber; towerIndex++) {
    std::ostringstream stringStream;
    stringStream << "tower"<<towerIndex;
    std::string keyString = stringStream.str();
    std::cout << keyString << std::endl;
    nlohmann::json singletower;
    for (int channel_index = 0; channel_index < 5; channel_index ++){
      nlohmann::json single_channel;
      std::ostringstream channelStream;
      stringStream << "channel"<<towerIndex;
      std::string channelString = stringStream.str();
      std::cout << channelString << std::endl;
      single_channel["errors"] = { {"crc_error", (int)rand()}, {"header_error", (int)rand()} };
      single_channel["pointers"] = { {"rd_pointer", (int)rand()}, {"wr_pointer", (int)rand()} };
      singletower[channelString] = single_channel;
    }
    doc[keyString] = singletower;
  }
  channel_error_->put({{"myjson", doc.dump()}}); 
  /*std::cout << "transfer debug :  " << std::endl << doc.dump() << std::endl;
  toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pubsub_pool_,2048);
  auto addr = static_cast<char*>(ref->getDataLocation());
  std::snprintf(addr, doc.dump().length()+1 , "%s", doc.dump().c_str());
  ref->setDataSize(doc.dump().length());

  pubsub_bus_->publish("electronicsupdate/"+appId_,ref);*/
}



void fsm::BCPSupervisor::fromHtoCAction(toolbox::Event::Reference e) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Begin");
  configureOnDetector("-999", "0");
  LOG4CPLUS_INFO(this->getApplicationLogger(), "End");
}

void fsm::BCPSupervisor::fromCtoRAction(toolbox::Event::Reference e) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "::goRunning Begin");
  LOG4CPLUS_INFO(this->getApplicationLogger(), "::goRunning Simulating some cpu time");
  std::chrono::milliseconds timespan(5000);
  std::this_thread::sleep_for(timespan);
  LOG4CPLUS_INFO(this->getApplicationLogger(), "::goRunning End");
}

const std::shared_ptr<api::restate::Response> fsm::BCPSupervisor::configureOnDetectorById(const api::restate::Request& r) {
  std::string conf_id = std::string(r.getArg("conf_id"));
  std::string towers = std::string(r.getArg("towers"));
  configureOnDetector(conf_id, towers);
  return r.createResponse("Done", api::restate::Utils::http_ok, "text/html; charset=utf-8");
}

void fsm::BCPSupervisor::configureOnDetector(std::string conf_id = "-999", std::string towers="0") {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Querying DB");
  auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
  auto connection = member->connect(DBInterfaceAddress_,{});
  if (conf_id == "-999") {
    std::string message = "/getLatestVFEConfigId";
    auto response = connection->get(message);
    conf_id = response->getContent();
  }
  std::string message = "/getVFEConfigById/" + conf_id;
  auto response = connection->get(message);
  // getContent is retrieving the registers from the DB
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Contact ELM");
  RPCSvc rpc;
  openRpcConnection(rpc, "pcminn34");
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Result content " << response->getContent());
  configureFesFromSingleFile(this->getApplicationLogger(), rpc, response->getContent(), towers);
  resetVfes(this->getApplicationLogger(), rpc, towers);
  prepareDTUAlign(this->getApplicationLogger(), rpc, response->getContent(), towers);
  auto connection_rogue = member->connect("pcminn34:5000",{});
  std::string message_rogue = "/roguecommand/align_BCP_with_sync_mode/1/" + towers;
  auto response_rogue = connection_rogue->get(message_rogue);
  message_rogue = "/roguecommand/optimize_pedestals/1/" + towers;
  connection_rogue->get(message_rogue);
//RELOAD THE REGISTERS, PULL THEM, AND GO THROUGH CONFIGURATION PROCESS AGAIN
}
