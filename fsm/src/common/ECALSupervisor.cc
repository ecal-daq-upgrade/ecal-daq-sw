#include "fsm/ECALSupervisor.h"
#include "api/restate/Utils.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "api/topics/Interface.h"
#include "api/topics/exception/Exception.h"


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/net/URN.h"
#include "chrono"
#include "yaml-cpp/yaml.h"
#include "dboracle.h"
#include "rpctest.h"
using namespace bcp::myoracle;

XDAQ_INSTANTIATOR_IMPL(fsm::ECALSupervisor);

fsm::ECALSupervisor::ECALSupervisor(xdaq::ApplicationStub * s): 
  fsm::Supervisor(s) {

    LOG4CPLUS_INFO(this->getApplicationLogger(), "Hello World!");
    auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
    member->registerResource("/responsetestchild", std::bind(&fsm::ECALSupervisor::responseTest, this, std::placeholders::_1), api::restate::Utils::http_method_get);
    member->registerResource("/sendstatechanged/{appid}/{newstate}", std::bind(&fsm::ECALSupervisor::sendStateChanged, this, std::placeholders::_1));
    //auto sub_cb = std::bind(&fsm::ECALSupervisor::onMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    //pubsub_bus_ = pubsub_member_->busAttach("mosquitto", sub_cb);
    LOG4CPLUS_INFO(this->getApplicationLogger(), "getProtocol " << member->getActiveConnections().size());    
    for (auto &it : member->getActiveConnections()) {
      LOG4CPLUS_INFO(this->getApplicationLogger(), "getProtocol " << it);
    }

    waitingAppStateChange_ = false;
    change_state_cmd_ = "";
    LOG4CPLUS_INFO(this->getApplicationLogger(), "Constructor done");
}

void fsm::ECALSupervisor::parseConfYaml() {
    LOG4CPLUS_INFO(this->getApplicationLogger(), "In parseConfYaml with conf file: " + confFile_.toString());
    YAML::Node config = YAML::LoadFile(confFile_.toString());
    YAML::Node apps = config["applications"];
    for (YAML::const_iterator it=apps.begin(); it!=apps.end(); ++it) {
      YAML::Node innerNode= it->second;
      std::string address = innerNode["address"].as<std::string>();
      std::string appClass = innerNode["class"].as<std::string>();
      if (appClass == "BCPSupervisor") {
        std::pair<std::string,std::string> mypair(it->first.as<std::string>(), "http://"+address);
        bcpSupervisorsAddresses_.insert(mypair);
        LOG4CPLUS_INFO(this->getApplicationLogger(), mypair.first + " " + mypair.second);
      } 
    } 
    LOG4CPLUS_INFO(this->getApplicationLogger(), "End of parseConfYaml");
}

//void fsm::ECALSupervisor::actionPerformed(xdata::Event& e) {


//  LOG4CPLUS_INFO(this->getApplicationLogger(), "In actionPerformed configuration file: " + confFile_.toString());	
//}
void fsm::ECALSupervisor::updateFunction() {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::updateFunction");
  channel_error_->put({{"temperature", 999.0 },{"units", "U+2103"}});

}

void fsm::ECALSupervisor::fromItoHAction (toolbox::Event::Reference e) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::fromItoHAction do something");
  this->broadcastChangeState();
}

void fsm::ECALSupervisor::fromHtoCAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::fromHtoCAction do something");
  broadcastChangeState();
}

void fsm::ECALSupervisor::fromCtoRAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::fromCtoRAction do something");
  broadcastChangeState();
}

void fsm::ECALSupervisor::fromRtoCAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::fromRtoCAction do something");
  broadcastChangeState();
}

void fsm::ECALSupervisor::fromCtoHAction (toolbox::Event::Reference e) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "We are in fsm::ECALSupervisor::fromCtoHAction do something");
  broadcastChangeState();
}

const std::shared_ptr<api::restate::Response> fsm::ECALSupervisor::responseTest(const api::restate::Request& r) {
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::ECALSupervisor::responseTest");
  auto myresponse = r.createResponse("Hello!", api::restate::Utils::http_ok, "text/html;charset=UTF-8");
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "HERE IN fsm::ECALSupervisor::responseTest after");
  return myresponse;
}
// When we receive a request to change the state of the ECALSupervisor, we fire the cmd so the ECALSupervisor will launch the inter state action.
// In this kind of functions, we broadcast to all the sub apps the action to change the state (broadcastChangeState and sendStateChanged).
// Then we check that all the apps have moved to the new state with (waitForStateChanged) and if there are no problems the ECALSupervisor close
// the action function and move to the new state.
// If one of the apps fails during the action to change the state, waitForStateChanged will raise an exception of missing app (if the app has not answered
// at all) or app in error (if the state of one of the apps is in failed)
// Rising an exception will move the ECALSupervisor in Failed state as well.
const std::shared_ptr<api::restate::Response> fsm::ECALSupervisor::changeState(const api::restate::Request& r) {

  std::string change_state_cmd = std::string(r.getArg("changestatecmd"));

  LOG4CPLUS_INFO(this->getApplicationLogger(), "Called ECALSupervisor::changeState with command " << change_state_cmd);
  std::string response = "No comments";
  change_state_cmd_ = change_state_cmd;
  //std::thread fireChangeStateThread(&fsm::ECALSupervisor::fireChangeState, this, change_state_cmd_);
  //fireChangeStateThread.detach();
  this->fireChangeState(change_state_cmd_);

  LOG4CPLUS_INFO(this->getApplicationLogger(), "Called ECALSupervisor::changeState after fire with cmd " << change_state_cmd);
  return r.createResponse(response, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
}
/*
void fsm::ECALSupervisor::onConnect(std::shared_ptr<api::topics::Bus> bus) {
    LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " is connected");
    if (bus->name() == "mosquitto") {
      bus->subscribe("appstate/+");
    } 
}*/


const std::shared_ptr<api::restate::Response> fsm::ECALSupervisor::sendStateChanged(const api::restate::Request& r) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Received stateChanged");
  std::string response = "Event accepted";
  std::string appid = std::string(r.getArg("appid"));
  std::string newstate = std::string(r.getArg("newstate"));
  
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Received stateChanged from " << appid << ", new state: " << newstate);
  if (!waitingAppStateChange_) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), "Received stateChanged from " << appid << ", new state: " << newstate << " NOT EXPECTED!");
    // Here I should check the new state of the unexpected app move the ECALSupervisor in error
  } else {
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Saving BCPSupervisor new state");
    std::pair<std::string,std::string> mypair(appid, newstate);
    bcpSupervisorsStates_.insert(mypair);
  }
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "sendStateChanged before return "<<api::restate::Utils::http_ok);
  auto theR = r.createResponse(response, api::restate::Utils::http_ok, "text/html;charset=UTF-8");
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "sendStateChanged after creating response");
  return theR;
}


void fsm::ECALSupervisor::sendChangeStateRequest(std::string bcpAddress) {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Sending command " << change_state_cmd_ << " to address " << bcpAddress);
  auto member = api::restate::getInterface(this->getApplicationContext())->join(this,this);
  try {
    auto connection = member->connect(bcpAddress,{});
    connection->get("/changestate/"+change_state_cmd_); 
  } catch (...) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), "Problem sending request with cmd " << change_state_cmd_ << " to " << bcpAddress);
  }
}

void fsm::ECALSupervisor::broadcastChangeState() {
  LOG4CPLUS_INFO(this->getApplicationLogger(), "About to broadcast following command: " << change_state_cmd_);

  std::map<std::string, std::string>::iterator mapIt = bcpSupervisorsAddresses_.begin();
  // I tell to the ECALSupervisor that we are sending the state change cmd to other apps
  // and it should expect sendStateChanged callback being called 
  waitingAppStateChange_ = true;
  while (mapIt != bcpSupervisorsAddresses_.end()) {
    // XDAQ HTTP requests are not asynchronous so we need to insert each of them in a thread
    std::thread source_thread(&fsm::ECALSupervisor::sendChangeStateRequest, this, mapIt->second);
    source_thread.detach();
    mapIt++;
  }

  waitForStateChanged();
  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of broadcastChangeState"); 
}

void fsm::ECALSupervisor::waitForStateChanged() {

  bool errorForMissingApp = false;
  bool errorForWrongState = false;
  int timeout = 10; // 10 seconds waiting for all apps to change state
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  // Starting with an empty bcpSupervisorsStates vector
  bcpSupervisorsStates_.clear();
  while(1) {
    if (bcpSupervisorsStates_.size() == bcpSupervisorsAddresses_.size()) {
      LOG4CPLUS_INFO(this->getApplicationLogger(), "All applications have sent the state changed. Stop waiting...");
      break;
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    if (std::chrono::duration_cast<std::chrono::seconds> (end-begin).count() > timeout) {
      LOG4CPLUS_ERROR(this->getApplicationLogger(), "Timeout, applications expected " << bcpSupervisorsAddresses_.size() << " found " << bcpSupervisorsStates_.size());
      for (auto &it : bcpSupervisorsAddresses_) {
        if (bcpSupervisorsStates_.find(it.first) == bcpSupervisorsStates_.end()) {
          LOG4CPLUS_ERROR(this->getApplicationLogger(), "Missing answer from app " << it.first);
          errorForMissingApp = true;
        }
      }
      break;
    }
    usleep(1);
  }
  toolbox::fsm::State nextState = getNextState(fsm_.getCurrentState(), change_state_cmd_);
  std::string nextStateName = fsm_.getStateName(nextState);
  for(auto &it : bcpSupervisorsStates_) {
    if(it.second != nextStateName) {
      errorForWrongState = true;
      LOG4CPLUS_ERROR(this->getApplicationLogger(), "App " << it.first << " found in wrong state " << it.second << " instead of " << nextStateName);
    }
  }
  waitingAppStateChange_ = false;
  if (errorForMissingApp || errorForWrongState) {
    // Here we should move ECALSupervisor in Error state
    LOG4CPLUS_ERROR(this->getApplicationLogger(), "errorForMissingApp " << errorForMissingApp << " errorForWrongState " << errorForWrongState);
    //XCEPT_RAISE (toolbox::fsm::exception::Exception, "Error for missing app or error for wrong state"); 
    XCEPT_RAISE (xdaq::exception::Exception, "Error for missing app or error for wrong state");
  }
}

/*void fsm::ECALSupervisor::onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref)
{
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "received message for topic '" << topic << "' from bus '" << bus->name() << "' of len " << ref->getDataSize());
    auto addr = static_cast<char*>(ref->getDataLocation());
    std::string mystring = addr;
    auto substring = mystring.substr(0,ref->getDataSize());
    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "message: "<< substring);

    if (topic.find("appstate") == 0) {
      // Split string like id:state
      std::string appId = topic.substr(topic.find("/")+1);
      std::string appState = substring;
      if (appId == this->getApplicationDescriptor()->getAttribute("id")) return;
      LOG4CPLUS_INFO(this->getApplicationLogger(), "Application with id "<< appId << " is in state " << appState);
      if (appState == "Initial") { // Initiaize map only at Initial state
        if (supervisorStateList_.find(appId) == supervisorStateList_.end()) { // Add element to list only once
          supervisorStateList_.insert(std::pair<std::string,std::string>(appId,appState));
        }
      }
      if (supervisorStateList_.find(appId) == supervisorStateList_.end()) { // Just checking that the app is in the list. Otherwise we have a big problem
        LOG4CPLUS_ERROR(this->getApplicationLogger(), "App " << appId << " not present in list while updating with status: " << appState);
        XCEPT_RAISE (toolbox::fsm::exception::Exception, "Error updating supervisorStateList_");
      } else {
        supervisorStateList_[appId] = appState;
      }
      if (fsm_.getStateName(fsm_.getCurrentState()) != "Failed" &&  appState == "Failed") {
        // breaking the main application as soon as a subapp goes into error.
        // sending the main app in error is not trivial, as we are in onMessage instead of in transition
        // plan is to make an external monitor to know how many subapps broke.
        //XCEPT_RAISE (toolbox::fsm::exception::Exception, "Error: one of the subapps is in error state.");
        this->fireChangeState("fail"); 
      }
    }

    ref->release();
    
}*/
