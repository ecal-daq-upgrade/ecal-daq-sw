#include "fsm/MyFsm.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

fsm::MyFsm::MyFsm() {
  fsm_.addState('I', "Initial",     this, &fsm::MyFsm::stateChanged);
  fsm_.addState('H', "Halt",        this, &fsm::MyFsm::stateChanged);
  fsm_.addState('C', "Configured",  this, &fsm::MyFsm::stateChanged);
  fsm_.addState('R', "Running",     this, &fsm::MyFsm::stateChanged);
  fsm_.addState('K', "Fake",		    this, &fsm::MyFsm::stateChanged);

  fsm_.addStateTransition('I', 'H', "fromItoH", this, &fsm::MyFsm::fromItoHAction);
  fsm_.addStateTransition('H', 'C', "fromHtoC", this, &fsm::MyFsm::fromHtoCAction);
  fsm_.addStateTransition('C', 'R', "fromCtoR", this, &fsm::MyFsm::fromCtoRAction);
  fsm_.addStateTransition('R', 'C', "fromRtoC", this, &fsm::MyFsm::fromRtoCAction);
  fsm_.addStateTransition('C', 'H', "fromCtoH", this, &fsm::MyFsm::fromCtoHAction);
  
  fsm_.setFailedStateTransitionAction(this, &fsm::MyFsm::failed);
  fsm_.setFailedStateTransitionChanged(this, &fsm::MyFsm::inFailed);

  fsm_.setInitialState('I');
  fsm_.setStateName('F', "Failed"); // give a name to the 'F' state (corresponding action is fail)
  fsm_.addStateTransition('I', 'F', "fail", this, &fsm::MyFsm::failed);
  fsm_.addStateTransition('H', 'F', "fail", this, &fsm::MyFsm::failed);
  fsm_.addStateTransition('C', 'F', "fail", this, &fsm::MyFsm::failed);
  fsm_.addStateTransition('R', 'F', "fail", this, &fsm::MyFsm::failed);

  
  fsm_.reset();
}

void fsm::MyFsm::fromItoHAction (toolbox::Event::Reference e)
{
  std::cout << "Event: [" << e->type() << "]" << std::endl;
  std::cout << "Action between transitions" << std::endl;
}

void fsm::MyFsm::fromHtoCAction (toolbox::Event::Reference e)
{
  std::cout << "Event: [" << e->type() << "]" << std::endl;
  std::cout << "Action between transitions" << std::endl;
}

void fsm::MyFsm::fromCtoRAction (toolbox::Event::Reference e)
{
  std::cout << "Event: [" << e->type() << "]" << std::endl;
  std::cout << "Action between transitions" << std::endl;
}

void fsm::MyFsm::fromRtoCAction (toolbox::Event::Reference e)
{
  std::cout << "Event: [" << e->type() << "]" << std::endl;
  std::cout << "Action between transitions" << std::endl;
}

void fsm::MyFsm::fromCtoHAction (toolbox::Event::Reference e)
{
  std::cout << "Event: [" << e->type() << "]" << std::endl;
  std::cout << "Action between transitions" << std::endl;
}

void fsm::MyFsm::failed (toolbox::Event::Reference e)
{
  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  std::cout << "Failure occurred when performing transition from: ";
  std::cout << fe.getFromState() <<  " to: " << fe.getToState();
  std::cout << ", exception: " << fe.getException().what();
  std::cout << std::endl;
}

void fsm::MyFsm::stateChanged (toolbox::fsm::FiniteStateMachine & fsm)
{
  // Reflect the new state
  std::cout << "Current state is: [" << fsm.getStateName (fsm.getCurrentState()) << "]" << std::endl;
}

void fsm::MyFsm::inFailed (toolbox::fsm::FiniteStateMachine & fsm)
{
  std::cout << "Entered state: [" << fsm.getStateName (fsm.getCurrentState()) << "]" << std::endl;
}

toolbox::fsm::State fsm::MyFsm::getNextState (toolbox::fsm::State s, std::string evtName)
{
  // Find final state, given starting state and event  
  toolbox::fsm::State myState;
  std::map<std::string, toolbox::fsm::State > transitions = fsm_.getTransitions(s);
  if (transitions.end() != transitions.find(evtName)){
    myState = transitions.find(evtName)->second;
  } else {
    std::cout << "Error: could not pair state to event" << std::endl;
    XCEPT_RAISE (toolbox::fsm::exception::Exception, "Error: could not pair state to event");
  } 
  return myState;
}

void fsm::MyFsm::fireChangeState(std::string action) {
  toolbox::Event::Reference e(new toolbox::Event(action,this));
  fsm_.fireEvent(e);
}
