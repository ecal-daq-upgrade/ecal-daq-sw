unzip instantclient-basic-linux.x64-19.23.0.0.0dbru.zip
mv instantclient_19_23 instantclient_19_23_lib
mv META-INF META-INF_lib
unzip instantclient-sdk-linux.x64-19.23.0.0.0dbru.zip
mv instantclient_19_23 instantclient_19_23_include
mv META-INF META-INF_include
cd instantclient_19_23_lib
ln -s libclntshcore.so.19.1 libclntshcore.so
